import React, {useEffect} from 'react';
import {
    BrowserRouter as Router,
    Switch,
    Route
} from "react-router-dom";
import Home from './components/Home/Home';
import Auth from './components/Auth/Auth';
import Layout from './components/Layout';
import Profile from './components/Profile/Profile';
import Receipes from './components/Receipes/Receipes';
import ReceipeDetail from './components/Receipes/ReceipeDetail/ReceipeDetail';
import PrivateRoute from './components/PrivateRoute';
import {useDispatch} from "react-redux";

import './index.css';
import {initSession} from "./actions/usersActions";
import ForgotPassword from "./components/Auth/ForgotPassword/ForgotPassword";
import Tchat from "./components/Tchat/Tchat";

const App = () => {
    const dispatch = useDispatch()

    useEffect(() => {
        dispatch(initSession())
    }, [dispatch])


    return (
        <Router>
            <Layout>
                <Switch>
                    <PrivateRoute exact path="/" component={Home}/>
                    <Route path="/auth/reset-password/:userId/:passwordToken" component={ForgotPassword}/>
                    <Route path="/auth" component={Auth}/>
                    <Route path="/user/profile/:id" component={Profile}/>
                    <Route path="/receipes/:id" component={ReceipeDetail} />
                    <Route path="/receipes" component={Receipes}/>
                    <PrivateRoute path="/tchat" component={Tchat}/>
                </Switch>
            </Layout>
        </Router>
    );
}

export default App;
