import axios from 'axios';


const API = axios.create({
    baseURL: process.env.REACT_APP_API_DOMMAIN,
    withCredentials: true,
})

export const signin = (formData) => API.post('/user/signin', formData);
export const signup = (formData) => API.post('/user/signup', formData);
export const logout = () => API.post('/user/logout');
export const initSession = () => API.get('/user/auth');
export const getUserProfile = (id) => API.get(`/user/profile/${id}`,);
export const editUserProfile = (id, formData) => {
    const bodyFormData = new FormData();

    for (let data in formData) {
        if (data === 'local') {
            bodyFormData.append(data, JSON.stringify(formData[data]))

        } else {
            bodyFormData.append(data, formData[data])
        }
    }

    return API.put(`/user/profile/${id}`, bodyFormData, {
        headers: {
            'accept': 'application/json',
            'Content-Type': 'multipart/form-data'
        }
    });
}
export const sendEmailForResetPassword = (email) => API.post('/user/auth/forgot-password',{email})
export const resetPassword = (id, token, formData) => API.post(`/user/auth/reset-password/${id}/${token}`, formData )

export const getReceipes = () => API.get('/receipe/')
export const addReceipe = (formData) => {
    const bodyFormData = new FormData();

    for (let data in formData) {
        bodyFormData.append(data, formData[data])
    }

    return API.post(`/receipe/add/`, bodyFormData, {
        headers: {
            'accept': 'application/json',
            'Content-Type': 'multipart/form-data'
        }
    });
}
export const updateReceipe = (formData, id) => {

    const bodyFormData = new FormData();
    for (let data in formData) {

        if ((data === 'picture' && formData[data].path )|| data === 'author') {
            bodyFormData.append(data, JSON.stringify(formData[data]))
        }  else {
            bodyFormData.append(data, formData[data])
        }
    }
    return API.put(`/receipe/${id}`, bodyFormData, {
        headers: {
            'accept': 'application/json',
            'Content-Type': 'multipart/form-data'
        }
    });
}
export const deleteReceipe = (id) => API.delete(`/receipe/${id}`)
export const getSelectedReceipe = (id) => API.get(`/receipe/${id}`)
export const addReceipeToFavorite = (receipeId) => API.put(`/receipe/addFavorites/`, {receipeId})
export const removeReceipeToFavorite = (receipeId) => API.put(`/receipe/removeFavorites/`, {receipeId})
export const getUserReceipes = (id) => API.get(`/receipe/user/${id}`);
export const getFavoritesReceipes = (id) => API.get(`/receipe/user/${id}/favorites/`);

export const getNamespaces = () => API.get('/tchat/namespaces');
export const getRoomsByNamespaceId = (id) => API.get(`/tchat/namespaces/${id}/rooms/`);
export const sendMessage = (message, roomId) => API.post(`/tchat/rooms/${roomId}/`, {message});