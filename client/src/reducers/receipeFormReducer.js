import {SET_FORM_VALUES_FOR_EDIT, RESET_FORM_VALUES} from "../actions/constants";

const initialState = {
    values: {
        title: '',
        ingredients: '',
        instructions: '',
        time: '',
        picture: ''
    },
    editMode: false
}
const receipeFormReducer = (state = initialState, action) => {
    switch (action.type) {
        case SET_FORM_VALUES_FOR_EDIT :
            return {...state, values: action.data, editMode: true};
        case  RESET_FORM_VALUES :
            return {...state, values: initialState.values, editMode: false}
        default:
            return state

    }
}

export default receipeFormReducer;