import {
    ADD_RECEIPE,
    ADD_RECEIPE_SUCCESS,
    ADD_RECEIPE_ERROR,
    GET_RECEIPES,
    GET_RECEIPES_ERROR,
    GET_RECEIPES_SUCCESS,
    UPDATE_RECEIPE,
    UPDATE_RECEIPE_ERROR,
    UPDATE_RECEIPE_SUCCESS,
    DELETE_RECEIPE,
    DELETE_RECEIPE_ERROR,
    DELETE_RECEIPE_SUCCESS,
    GET_SELECTED_RECEIPE,
    GET_SELECTED_RECEIPE_ERROR,
    GET_SELECTED_RECEIPE_SUCCESS,
    ADD_RECEIPE_TO_FAVORITE_SUCCESS,
    ADD_RECEIPE_TO_FAVORITE,
    ADD_RECEIPE_TO_FAVORITE_ERROR,
    REMOVE_RECEIPE_TO_FAVORITE_SUCCESS, REMOVE_RECEIPE_TO_FAVORITE_ERROR, REMOVE_RECEIPE_TO_FAVORITE
} from "../actions/constants";

import {SUCCESS, ERROR} from "../components/Errors/constants";

const initialState = {
    receipes: [],
    isLoading: true,
    selectedReceipe: null,
    message: {
        active: false,
        type: '',
        message: ''
    }
}
const receipeReducer = (state = initialState, action) => {
    switch (action.type) {
        case REMOVE_RECEIPE_TO_FAVORITE:
        case ADD_RECEIPE_TO_FAVORITE :
        case GET_SELECTED_RECEIPE :
        case GET_RECEIPES :
        case UPDATE_RECEIPE :
        case ADD_RECEIPE :
        case DELETE_RECEIPE :
            return {
                ...state,
                isLoading: true,
                message: {
                    active: false,
                    type: '',
                    message: ''
                }
            };

        case GET_RECEIPES_SUCCESS :
            return {
                ...state,
                receipes: [...action.data.receipes],
                isLoading: false,
                message: {
                    active: false,
                    type: '',
                    message: ''
                }
            }
        case UPDATE_RECEIPE_SUCCESS:
            return {
                ...state, receipes:
                    state.receipes.map(r => {
                        if (r._id === action.data.receipe._id) {
                            return action.data.receipe
                        }
                        return r

                    }),
                selectedReceipe: state.selectedReceipe && (state.selectedReceipe._id.toString() === action.data.receipe._id.toString()) ? action.data.receipe : state.selectedReceipe,
                isLoading: false,
                message: {
                    active: true,
                    type: SUCCESS,
                    message: `La recette ${action.data.receipe.title} à bien été mise à jour.`
                }
            }

        case DELETE_RECEIPE_SUCCESS:
            return {
                ...state, receipes:
                    state.receipes.filter(r => {
                        return r._id !== action.data.receipe._id
                    }),
                isLoading: false,
            }
        case ADD_RECEIPE_SUCCESS :
            const receipes = [...state.receipes]
            receipes.push(action.data.receipe)
            return {
                ...state,
                receipes: receipes,
                isLoading: false,
                message: {
                    active: true,
                    type: SUCCESS,
                    message: `La recette ${action.data.receipe.title} à bien été ajoutée.`
                }
            };

        case REMOVE_RECEIPE_TO_FAVORITE_SUCCESS :
            return {
                ...state,
                receipes: state.receipes.map(receipe => {
                    if (receipe._id === action.data.disLikedReceipe._id) {
                        return action.data.disLikedReceipe
                    }
                    return receipe;
                }),
                selectedReceipe: action.data.disLikedReceipe,
                isLoading: false,
                message: {
                    active: false,
                    type: '',
                    message: ''
                }
            };

        case ADD_RECEIPE_TO_FAVORITE_SUCCESS :

            return {
                ...state,
                receipes: state.receipes.map(receipe => {
                    if (receipe._id === action.data.likedReceipe._id) {
                        return action.data.likedReceipe
                    }
                    return receipe;
                }),
                selectedReceipe: action.data.likedReceipe,
                isLoading: false,
                message: {
                    active: false,
                    type: '',
                    message: ''
                }
            };

        case GET_SELECTED_RECEIPE_SUCCESS :
            return {
                ...state,
                selectedReceipe: action.data.selectedReceipe,
                isLoading: false,
                message: {
                    active: false,
                    type: '',
                    message: ''
                }
            };

        case REMOVE_RECEIPE_TO_FAVORITE_ERROR:
        case ADD_RECEIPE_TO_FAVORITE_ERROR :
        case GET_SELECTED_RECEIPE_ERROR :
        case GET_RECEIPES_ERROR :
        case UPDATE_RECEIPE_ERROR :
        case ADD_RECEIPE_ERROR :
        case DELETE_RECEIPE_ERROR :
            return {
                ...state,
                isLoading: false,
                message: {
                    active: true,
                    type: ERROR,
                    message: action.error
                }
            };
        default:
            return state
    }
}

export default receipeReducer;