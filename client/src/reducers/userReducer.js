import {
    LOGOUT,
    LOGOUT_SUCCESS,
    LOGOUT_ERROR,
    SIGNIN,
    SIGNIN_ERROR,
    SIGNIN_SUCCESS,
    SIGNUP,
    SIGNUP_ERROR,
    SIGNUP_SUCCESS,
    INIT_SESSION,
    INIT_SESSION_SUCCESS,
    INIT_SESSION_ERROR,
    GET_USER_PROFILE,
    GET_USER_PROFILE_SUCCESS,
    GET_USER_PROFILE_ERROR,
    EDIT_USER_PROFILE,
    EDIT_USER_PROFILE_SUCCESS,
    EDIT_USER_PROFILE_ERROR,
    SEND_EMAIL_FOR_RESET_PASSWORD_SUCCESS,
    SEND_EMAIL_FOR_RESET_PASSWORD_ERROR,
    SEND_EMAIL_FOR_RESET_PASSWORD,
    RESET_PASSWORD,
    RESET_PASSWORD_ERROR,
    RESET_PASSWORD_SUCCESS,
    GET_USER_RECEIPES_SUCCESS,
    GET_USER_RECEIPES_ERROR,
    GET_USER_RECEIPES,
    GET_FAVORITES_RECEIPES_SUCCESS,
    GET_FAVORITES_RECEIPES_ERROR,
    GET_FAVORITES_RECEIPES
} from "../actions/constants";

import {SUCCESS, ERROR} from "../components/Errors/constants";

const initialState = {
    user: {
        user: null,
        isAuthenticated: false
    },
    isLoading: false,
    message: {
        active: false,
        type: '',
        message: ''
    },
    profile: null,
    receipes: [],
    favoritesReceipes: []
}
const userReducer = (state = initialState, action) => {

    switch (action.type) {

        case INIT_SESSION :
        case GET_USER_PROFILE:
            return {
                ...state, isLoading: true
            };
        case SIGNUP :
        case RESET_PASSWORD :
        case SIGNIN :
        case LOGOUT :
        case SEND_EMAIL_FOR_RESET_PASSWORD :
        case EDIT_USER_PROFILE:
            return {
                ...state, isLoading: true, message: {
                    active: false,
                    type: '',
                    message: ''
                }
            };

        case SIGNIN_SUCCESS:
            return {
                ...state, user: action.data, isLoading: false, message: {
                    active: true,
                    type: SUCCESS,
                    message: 'Vous êtes connecté.'
                }
            };
        case SIGNUP_SUCCESS:
            return {
                ...state, user: action.data, isLoading: false, message: {
                    active: true,
                    type: SUCCESS,
                    message: 'Votre inscription est terminée.'
                }
            };

        case RESET_PASSWORD_SUCCESS :
            return {
                ...state, isLoading: false, message: {
                    active: true,
                    type: SUCCESS,
                    message: 'Votre mot de passe est modifié'
                }
            }

        case GET_USER_RECEIPES_ERROR:
        case GET_FAVORITES_RECEIPES_ERROR:
        case GET_USER_PROFILE_ERROR:
        case EDIT_USER_PROFILE_ERROR:
        case INIT_SESSION_ERROR:
        case SEND_EMAIL_FOR_RESET_PASSWORD_ERROR:
        case LOGOUT_ERROR:
        case SIGNIN_ERROR:
        case SIGNUP_ERROR:
        case RESET_PASSWORD_ERROR :
            return {
                ...state, isLoading: false,
                message: {
                    active: true,
                    type: ERROR,
                    message: action.error
                }
            };

        case LOGOUT_SUCCESS:
            return {
                ...state, user: action.data, isLoading: false, error: null,
                message: {
                    active: true,
                    type: SUCCESS,
                    message: 'Vous êtes déconnecté.'
                }
            };

        case INIT_SESSION_SUCCESS :
            return {
                ...state, user: action.data, isLoading: false, message: {
                    active: false,
                    type: '',
                    message: ''
                }
            };

        case EDIT_USER_PROFILE_SUCCESS :
            return {
                ...state,
                user: action.data,
                profile: action.data.user,
                isLoading: false,
                message: {
                    active: true,
                    type: SUCCESS,
                    message: 'Votre profil est mis à jour.'
                }
            }
        case SEND_EMAIL_FOR_RESET_PASSWORD_SUCCESS :
            return {
                ...state,
                isLoading: false,
                message: {
                    active: true,
                    type: SUCCESS,
                    message: `Vous avez recu un message sur l'adresse mail ${action.data.user.local.email} avec les instructions pour reinitialiser votre mot de passe`
                }
            }
        case GET_USER_PROFILE_SUCCESS :
            return {...state, profile: action.data.user, isLoading: false}

        case GET_USER_RECEIPES_SUCCESS:
            return {
                ...state,
                receipes: action.data.userReceipes,
            };

        case GET_FAVORITES_RECEIPES_SUCCESS:
            console.log(action.data)
            return {
                ...state,
                favoritesReceipes: action.data.favoritesReceipes,
            };

        case GET_USER_RECEIPES:
        case GET_FAVORITES_RECEIPES:
        default:
            return state;
    }

}

export default userReducer;