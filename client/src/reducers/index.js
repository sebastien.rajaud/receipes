import {combineReducers} from "redux";
import userReducer from './userReducer';
import receipeReducer from './receipeReducer';
import receipeFormReducer from './receipeFormReducer';
import tchatReducer from './tchatReducer';

export default combineReducers({
    userReducer,
    receipeReducer,
    receipeFormReducer,
    tchatReducer
})