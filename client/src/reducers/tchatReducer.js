import {
    CHANGE_ROOM_SUCCESS,
    INIT_HISTORY_SUCCESS,
    INIT_TCHAT_SUCCESS,
    SEND_MESSAGE_SUCCESS,
    UPDATE_MESSAGE_SUCCESS,
    CHANGE_NAMESPACE_SUCCESS
} from "../actions/constants";

const initialState = {
    activeNameSpaceSocket: null,
    activeRoom: null,
    namespaces: [],
    rooms: [],
    messages: []
}

const tchatReducer = (state = initialState, action) => {
    switch (action.type) {
        case INIT_TCHAT_SUCCESS:
            return {
                namespaces: action.namespaces,
                rooms: action.rooms,
                activeRoom: action.activeRoom,
                messages: action.messages
            }
        case SEND_MESSAGE_SUCCESS :
            return {
                ...state,
                messages: [...state.messages, action.data]
            }

        case UPDATE_MESSAGE_SUCCESS :

            return {
                ...state,
                messages: [...state.messages, action.data]
            }

        case INIT_HISTORY_SUCCESS :

            return {
                ...state,
                messages: [...action.data]
            }

        case CHANGE_ROOM_SUCCESS :
            return {
                ...state,
                activeRoom: action.data
            }

        case CHANGE_NAMESPACE_SUCCESS :

            return {
                ...state,
                rooms: action.data
            }
        default :
            return state;
    }
}

export default tchatReducer;