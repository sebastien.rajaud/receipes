import {makeStyles} from "@material-ui/core";

export default makeStyles( theme => ({
    paper: {
        maxWidth: '500px',
        margin: '80px auto',
        padding: theme.spacing(6)
    }
}))