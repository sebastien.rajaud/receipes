import {makeStyles} from "@material-ui/core";

export default makeStyles( theme => ({
    titleForm: {
        marginBottom: theme.spacing(5)
    },
    textField: {
        width: '100%'
    },
    formActions: {
        marginTop: theme.spacing(2),
    },
    paper: {
        maxWidth: '500px',
        margin: '80px auto',
        padding: theme.spacing(6)
    }
}))