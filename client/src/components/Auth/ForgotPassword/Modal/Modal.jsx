import {IconButton} from "@material-ui/core";
import React from "react";
import ReactDOM from 'react-dom';
import useStyles from './styles';
import {Container, Paper} from "@material-ui/core";
import CloseIcon from '@material-ui/icons/Close';

const Modal = ({children, handleClose}) => {
    const classes = useStyles();

    return ReactDOM.createPortal(
        <div className={classes.modalOuter}>

            <div className={classes.modalOverlay}></div>
            <Container>
                <Paper className={classes.paper}>
                    <IconButton onClick={handleClose} className={classes.closeIcon}>
                        <CloseIcon color="primary"/>
                    </IconButton>
                    {children}
                </Paper>
            </Container>
        </div>,
        document.querySelector('#modal-root')
    );

}

export default Modal;