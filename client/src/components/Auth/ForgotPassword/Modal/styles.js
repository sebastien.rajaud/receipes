import {makeStyles} from "@material-ui/core";

export default makeStyles(theme => ({
    modalOuter: {
        position: "fixed",
        top: 0,
        left: 0,
        bottom: 0,
        right: 0,
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: "column",
        display: "flex",
        zIndex: 5
    },
    modalOverlay: {
        position: "absolute",
        top: 0,
        left: 0,
        bottom: 0,
        right: 0,
        background: 'black',
        opacity: '0.5',
        zIndex:-1
    },
    paper: {
        maxWidth: 650,
        margin: 'auto',
        padding: theme.spacing(4)
    },
    closeIcon: {
        marginRight: 0,
        marginLeft: "auto",
        display: "block"
    }

}))