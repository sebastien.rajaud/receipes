import React, { useState} from "react";
import {
    Button,
    Grid,
    IconButton,
    InputAdornment,
    TextField,
    Typography,
    CircularProgress,
    Paper
} from '@material-ui/core';
import useStyles from './styles';
import Visibility from '@material-ui/icons/Visibility';
import VisibilityOff from '@material-ui/icons/VisibilityOff';
import {useDispatch, useSelector} from "react-redux";
import {Link, useParams, useHistory} from 'react-router-dom';
import {resetPassword} from "../../../actions/usersActions";

const ForgotPassword = () => {

    const initialState = {
        password: '',
        confirmPassword: '',
    }
    const [formData, setFormData] = useState(initialState);
    const [showPassword, setShowPassword] = useState(false);

    const isLoading = useSelector(state => state.userReducer.isLoading)
    const dispatch = useDispatch();
    const history = useHistory();

    const {userId, passwordToken} = useParams();


    const handleSubmit = async (e) => {
        e.preventDefault();
        const response =  await dispatch(resetPassword(userId, passwordToken, formData, history));
        console.log(response)
    }

    const handleChange = (e) => {
        setFormData({...formData, [e.target.name]: e.target.value})
    }

    const classes = useStyles();

    return (
        <form onSubmit={handleSubmit}>
            {isLoading ? <CircularProgress/> : (
                <Paper className={classes.paper}>
                    <Typography
                        className={classes.titleForm}
                        variant="h4"
                        color="textPrimary"
                        gutterBottom>Modification du mot de passe</Typography>
                    <Grid container spacing={2}>

                        <Grid item sm={12} xs={12}>
                            <TextField className={classes.textField}
                                       id="password"
                                       onChange={handleChange}
                                       value={formData.password}
                                       type={showPassword ? "text" : "password"}
                                       label="Nouveau mot de passe"
                                       name="password"
                                       variant="outlined"
                                       InputProps={{
                                           endAdornment: (
                                               <InputAdornment>
                                                   <IconButton
                                                       onClick={() => {
                                                           setShowPassword(oldState => !oldState)
                                                       }}
                                                   >
                                                       {showPassword ? <Visibility/> : <VisibilityOff/>}
                                                   </IconButton>
                                               </InputAdornment>
                                           )
                                       }}

                            />
                        </Grid>
                        <Grid item sm={12} xs={12}>
                            <TextField className={classes.textField}
                                       id="confirmPassword"
                                       onChange={handleChange}
                                       value={formData.confirmPassword}
                                       type={showPassword ? "text" : "password"}
                                       label="Confirmez votre nouveau mot de passe"
                                       name="confirmPassword"
                                       variant="outlined"
                                       InputProps={{
                                           endAdornment: (
                                               <InputAdornment>
                                                   <IconButton
                                                       onClick={() => {
                                                           setShowPassword(oldState => !oldState)
                                                       }}
                                                   >
                                                       {showPassword ? <Visibility/> : <VisibilityOff/>}
                                                   </IconButton>
                                               </InputAdornment>
                                           )
                                       }}

                            />

                        </Grid>

                        <Grid container justify="space-between" className={classes.formActions}>
                            <Button type="submit"
                                    variant="contained"
                                    size="large"
                                    color="primary">Confirmer</Button>
                            <Button
                                variant="outlined"
                                color="secondary"
                                component={Link}
                                to="/auth"
                            >Retour</Button>
                        </Grid>

                    </Grid>
                </Paper>
            )}

        </form>
    )
}

export default ForgotPassword;