import React, {useState} from "react";
import {Button, Grid, IconButton, InputAdornment, TextField, Typography, CircularProgress} from '@material-ui/core';
import useStyles from './styles';
import Visibility from '@material-ui/icons/Visibility';
import VisibilityOff from '@material-ui/icons/VisibilityOff';
import {useDispatch, useSelector} from "react-redux";
import {signin, signup} from "../../../actions/usersActions";
import { useHistory} from "react-router-dom";
import Modal from '../ForgotPassword/Modal/Modal';
import { sendEmailForResetPassword } from '../../../actions/usersActions';

const Form = () => {

    const initialState = {
        firstName: '',
        lastName: '',
        address: '',
        city: '',
        email: '',
        password: '',
        confirmPassword: ''
    }
    const [isSignin, setIsSignin] = useState(false)
    const [formData, setFormData] = useState(initialState);
    const [showModal, setShowModal] = useState(false);
    const [showPassword, setShowPassword] = useState(false);
    const [modalEmail, setModalEmail] = useState('');

    const isLoading = useSelector(state => state.userReducer.isLoading)


    const dispatch = useDispatch();
    const history = useHistory();

    const handleSubmit = (e) => {
        e.preventDefault();
        console.log('Master submit')
        if (isSignin) {
            dispatch(signin(formData, history));
        } else {
            dispatch(signup(formData, history))
        }

        clearForm();
    }

    const handleChange = (e) => {
        setFormData({...formData, [e.target.name]: e.target.value})
    }

    const clearForm = () => {
        setFormData(initialState);
    }

    const handleModal = () => {
        setShowModal(state => !state)
    }

    const handleSubmitEmailPassForgot = (e) => {
        e.preventDefault();
        dispatch(sendEmailForResetPassword(modalEmail))
        setShowModal(false);
        setModalEmail('');
    }

    const classes = useStyles();

    return (
        <form onSubmit={handleSubmit}>
            <Typography
                className={classes.titleForm}
                variant="h4"
                color="textPrimary"
                gutterBottom>{isSignin ? 'Signin' : 'Signup'}</Typography>

            {isLoading ? <CircularProgress/> : <Grid container spacing={2}>
                {!isSignin && (
                    <>
                        <Grid item sm={6} xs={12}>
                            <TextField className={classes.textField}
                                       onChange={handleChange}
                                       value={formData.firstName}
                                       id="firstName" label="Prénom" name="firstName"
                                       variant="outlined"/>
                        </Grid>
                        <Grid item sm={6} xs={12}>
                            <TextField className={classes.textField}
                                       onChange={handleChange}
                                       value={formData.lastName}
                                       id="lastName" label="Nom" name="lastName"
                                       variant="outlined"/>
                        </Grid>
                        <Grid item sm={6} xs={12}>
                            <TextField className={classes.textField}
                                       onChange={handleChange}
                                       value={formData.address}
                                       id="address" label="Adresse" name="address"
                                       variant="outlined"/>
                        </Grid>
                        <Grid item sm={6} xs={12}>
                            <TextField className={classes.textField}
                                       onChange={handleChange}
                                       value={formData.city}
                                       id="city" label="Ville" name="city"
                                       variant="outlined"/>
                        </Grid>
                    </>
                )

                }

                <Grid item sm={12} xs={12}>
                    <TextField className={classes.textField}
                               id="email"
                               onChange={handleChange}
                               value={formData.email}
                               label="Email"
                               name="email"
                               variant="outlined"/>
                </Grid>
                <Grid item sm={12} xs={12}>
                    <TextField className={classes.textField}
                               id="password"
                               onChange={handleChange}
                               value={formData.password}
                               type={showPassword ? "text" : "password"}
                               label="Password"
                               name="password"
                               variant="outlined"
                               InputProps={{
                                   endAdornment: (
                                       <InputAdornment>
                                           <IconButton
                                               onClick={() => {
                                                   setShowPassword(oldState => !oldState)
                                               }}
                                           >
                                               {showPassword ? <Visibility/> : <VisibilityOff/>}
                                           </IconButton>
                                       </InputAdornment>
                                   )
                               }}

                    />
                </Grid>
                {!isSignin && <Grid item sm={12} xs={12}>
                    <TextField className={classes.textField}
                               id="confirmPassword"
                               onChange={handleChange}
                               value={formData.confirmPassword}
                               type={showPassword ? "text" : "password"}
                               label="Confirmez votre mot de passe"
                               name="confirmPassword"
                               variant="outlined"
                               InputProps={{
                                   endAdornment: (
                                       <InputAdornment>
                                           <IconButton
                                               onClick={() => {
                                                   setShowPassword(oldState => !oldState)
                                               }}
                                           >
                                               {showPassword ? <Visibility/> : <VisibilityOff/>}
                                           </IconButton>
                                       </InputAdornment>
                                   )
                               }}

                    />
                </Grid>}

                {isSignin && (
                    <Grid item sm={12} xs={12}>
                        <Typography

                            onClick={handleModal}
                            variant="overline" color="secondary"
                            className={classes.forgotPassword}>Mot de passe oublié</Typography>

                        {showModal && (
                            <Modal handleClose={() => {
                                setShowModal(false)
                            }}>
                                <Typography variant="h6" color="textPrimary" gutterBottom>Rensignez votre adresse
                                    email</Typography>
                                    <Grid container spacing={1}>
                                        <Grid item xs={10}>
                                            <TextField
                                                id="email"
                                                label="Email"
                                                name="modalEmail"
                                                fullWidth
                                                value={modalEmail}
                                                onChange={(e) => setModalEmail(e.target.value)}
                                                variant="outlined"/>
                                        </Grid>
                                        <Grid item xs={2}>
                                            <Button
                                                onClick={handleSubmitEmailPassForgot}
                                                className={classes.buttonModal} type="submit" variant="contained"
                                                    color="primary">OK</Button>
                                        </Grid>
                                    </Grid>

                            </Modal>
                        )}
                    </Grid>
                )}
                <Grid container justify="space-between" className={classes.formActions}>
                    <Button type="submit"
                            variant="contained"
                            size="large"
                            color="primary">{isSignin ? 'Signin' : 'Signup'}</Button>

                    <Button
                        variant="text"
                        onClick={() => setIsSignin(oldState => !oldState)}
                    >{isSignin ? 'Inscrivez-vous' : 'Vous avez un compte ? Connectez vous'}</Button>
                </Grid>

            </Grid>}

        </form>
    )
}

export default Form;