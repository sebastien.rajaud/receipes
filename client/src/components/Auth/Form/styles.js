import {makeStyles} from "@material-ui/core";

export default makeStyles( theme => ({
    titleForm: {
        marginBottom: theme.spacing(5)
    },
    textField: {
        width: '100%'
    },

    formActions: {
        marginTop: theme.spacing(2),
    },
    forgotPassword: {
        textDecoration:'none',
        '&:hover' : {
            color: theme.palette.text.primary
        }
    },
    buttonModal: {
        height: '100%'
    }

}))