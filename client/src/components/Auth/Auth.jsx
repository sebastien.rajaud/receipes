import React from 'react';
import {Container, Paper} from "@material-ui/core";
import Form from './Form/Form';
import useStyles from './styles';
import {useSelector} from "react-redux";
import {Redirect} from 'react-router-dom';

const Auth = () => {
    const classes = useStyles();
    const isAuthenticated = useSelector(state => state.userReducer.user.isAuthenticated);

    return (
        <>
            {isAuthenticated === true ? <Redirect to="/"/> : (
                <Container>
                    <Paper className={classes.paper}>
                        <Form/>
                    </Paper>
                </Container>
            )}

        </>

    )
}

export default Auth;