import {Route, Redirect} from 'react-router-dom';
import {useSelector} from "react-redux";

const PrivateRoute = ({component: Component, ...otherProps}) => {

    const isAuthenticated = useSelector(state => state.userReducer.user.isAuthenticated);
    return (
        <Route
            {...otherProps}
            render={props => (
                isAuthenticated ? <Component {...props} /> : <Redirect to="/auth"/>
            )}

        />
    )
}

export default PrivateRoute;