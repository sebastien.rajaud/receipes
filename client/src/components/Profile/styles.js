import {makeStyles} from "@material-ui/core";

export default makeStyles( theme => ({
    paper: {
        padding: theme.spacing(4),
        textAlign: 'center'
    },
    avatar: {
        width: '100px',
        height: '100px',
        marginLeft: 'auto',
        marginRight: 'auto',
        marginBottom: theme.spacing(4),
       '& svg' :{
          fontSize: '75px'
       }
    },
    userName: {
        marginBottom: theme.spacing(2),
        marginRight: theme.spacing(1),
        display:'inline-block'
    },
    address: {
        marginBottom: theme.spacing(2),
        display:'inline-block',
        marginRight: theme.spacing(1),
    },
    icons: {
        fontSize: '30px',
        display: 'block',
        margin: 'auto'
    },
    editButton: {
        marginTop: theme.spacing(4),
        marginRight: theme.spacing(1),
        marginLeft: theme.spacing(1),
        '& svg' : {
            fontSize: '16px',
            marginRight: '5px'
        }
    },
    chipSuccess : {
        marginTop: theme.spacing(1),
        background: theme.palette.success.main,
        color: theme.palette.common.white
    },
    chipAlert : {
        marginTop: theme.spacing(1),
        background: theme.palette.warning.main,
        color: theme.palette.common.white
    }
}))