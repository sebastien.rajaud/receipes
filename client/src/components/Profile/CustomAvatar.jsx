import React from "react";
import {Avatar} from "@material-ui/core";
import PersonIcon from "@material-ui/icons/Person";
import {getMediaUrl} from "../../utils/Media";

const CustomAvatar = ({alternativeText, mediaUrl, ...otherProps}) => {

    if (mediaUrl && mediaUrl !== '') {
        return <Avatar {...otherProps} alt={alternativeText}
                       src={getMediaUrl(mediaUrl)} />
    }
    return (

        <Avatar {...otherProps} >
            <PersonIcon/>
        </Avatar>
    );

}

export default CustomAvatar;