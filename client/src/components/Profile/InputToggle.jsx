import React from 'react';
import {Typography, TextField} from "@material-ui/core";

const InputToggle = ({toggle, children, name, handleChange, variant,type, ...otherProps}) => {
    return (
        <>
        {

            toggle ? (
                <TextField fullWidth type={ type ? type :'text'} {...otherProps} value={children} name={name} onChange={handleChange} />
            ) : (

                <Typography
                    variant={variant}
                    {...otherProps}
                >{children}</Typography>
            )
        }
            </>
    )
}
export default InputToggle;