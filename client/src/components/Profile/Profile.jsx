import React, {useEffect, useState} from 'react';
import {CircularProgress, Grid, Paper, Button} from "@material-ui/core";
import {useDispatch, useSelector} from "react-redux";
import {useParams} from 'react-router-dom';
import {getUserProfile, editUserProfile} from "../../actions/usersActions";
import useStyles from './styles';
import RoomIcon from '@material-ui/icons/Room';
import EmailIcon from '@material-ui/icons/Email';
import EditIcon from '@material-ui/icons/Edit';
import CheckIcon from '@material-ui/icons/Check';
import CloseIcon from '@material-ui/icons/Close';
import InputToggle from "./InputToggle";
import ImageToggle from './ImageToggle';
import CustomAvatar from "./CustomAvatar";
import Chip from "@material-ui/core/Chip";
import ProfileTabs from "./ProfilTabs/ProfileTabs";


const Profile = () => {
    const {id} = useParams();
    const isLoading = useSelector(state => state.userReducer.isLoading);
    const profile = useSelector(state => state.userReducer.profile);
    const user = useSelector(state => state.userReducer.user.user);


    const dispatch = useDispatch();
    const classes = useStyles();

    const [isEditing, setIsEditing] = useState(false);
    const [formData, setFormData] = useState({
        _id: '',
        firstName: '',
        lastName: '',
        address: '',
        local: {
            email: '',
            password: ''
        },
        city: '',
        avatar: {},
        description: '',
    })

    useEffect(() => {
        dispatch(getUserProfile(id))
    }, [dispatch, id])

    useEffect(() => {
        if (profile) {
            setFormData({...profile})
        }
    }, [profile])

    const handleTextChange = (e) => {

        if (e.target.name === 'email') {
            setFormData(state => (
                {...state, local: {...state.local, [e.target.name]: e.target.value}}
            ));
        } else {
            setFormData(state => ({...state, [e.target.name]: e.target.value}));
        }
    }

    const handleImageChange = (image) => {
        setFormData(state => ({
            ...state,
            avatar: image
        }))
    }

    const cancelEdit = () => {
        setIsEditing(oldState => !oldState);
        setFormData({...profile});
    }

    const handleSubmitEdition = () => {
        dispatch(editUserProfile(formData._id, formData));
        setIsEditing(oldState => !oldState);
    }

    return (
        <>
            {isLoading ? <CircularProgress/> : (

                <Grid container spacing={4}>
                    <Grid item xs={12} sm={8}>
                        <ProfileTabs/>
                    </Grid>
                    <Grid item xs={12} sm={4}>
                        <Paper className={classes.paper}>
                            {profile && <>
                                <ImageToggle
                                    toggle={isEditing}
                                    name="avatar"
                                    component={CustomAvatar}
                                    alternativeText={`${profile.firstName} ${profile.lastName}`}
                                    mediaUrl={formData.avatar ? formData.avatar.path : ''}
                                    imageClasses={classes.avatar}
                                    handleChange={handleImageChange}
                                />
                                <InputToggle
                                    label="Prénom"
                                    variant="h4"
                                    name="firstName"
                                    className={classes.userName}
                                    toggle={isEditing}
                                    handleChange={handleTextChange}
                                >{formData.firstName}</InputToggle>
                                <InputToggle
                                    label="Nom"
                                    variant="h4"
                                    name="lastName"
                                    className={classes.userName}
                                    toggle={isEditing}
                                    handleChange={handleTextChange}
                                >{formData.lastName}</InputToggle>

                                <RoomIcon className={classes.icons} color="secondary"/>
                                <InputToggle
                                    label="Adresse"
                                    variant="subtitle1"
                                    name="address"
                                    className={classes.address}
                                    toggle={isEditing}
                                    handleChange={handleTextChange}
                                >{formData.address}</InputToggle>
                                <InputToggle
                                    label="Ville"
                                    variant="subtitle1"
                                    name="city"
                                    className={classes.address}
                                    toggle={isEditing}
                                    handleChange={handleTextChange}
                                >{formData.city}</InputToggle>


                                <EmailIcon className={classes.icons} color="secondary"/>
                                <InputToggle
                                    label="Email"
                                    variant="subtitle2"
                                    name="email"
                                    className={classes.email}
                                    toggle={isEditing}
                                    handleChange={handleTextChange}
                                >{formData.local.email}</InputToggle>
                                <div>{user && (user.local.emailVerified ?
                                    <Chip size="small" label="Verifié" className={classes.chipSuccess}/> :
                                    <Chip size="small" label="Non verifié" className={classes.chipAlert}/>)}</div>
                                {user && user._id === profile._id && (
                                    <div>
                                        {isEditing ? (
                                            <>
                                                <Button
                                                    onClick={handleSubmitEdition}
                                                    variant="contained"
                                                    color="primary"
                                                    className={classes.editButton}>
                                                    <CheckIcon/> OK
                                                </Button>
                                                <Button
                                                    onClick={cancelEdit}
                                                    variant="outlined"
                                                    color="secondary"
                                                    className={classes.editButton}>
                                                    <CloseIcon/> Cancel
                                                </Button>
                                            </>
                                        ) : (<Button
                                            onClick={() => setIsEditing(oldState => !oldState)}
                                            variant="outlined"
                                            color="primary"
                                            className={classes.editButton}>
                                            <EditIcon/> Edit
                                        </Button>)
                                        }

                                    </div>
                                )}
                            </>}
                        </Paper>
                    </Grid>
                </Grid>

            )}
        </>
    )
}


export default Profile;