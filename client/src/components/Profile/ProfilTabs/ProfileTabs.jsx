import {Box, Grid, Tab, Tabs, Typography} from "@material-ui/core";
import React, {useState, useEffect} from "react";
import {useDispatch, useSelector} from "react-redux";
import {getUserReceipes, getFavoritesReceipes} from '../../../actions/receipesActions';
import ReceipeItem from "../../Receipes/Receipe/ReceipeItem";

const ProfileTabs = () => {
    const [activeTab, setActiveTab] = useState(0);
    const dispatch = useDispatch();
    const user = useSelector(state => state.userReducer.user.user);
    const receipes = useSelector(state => state.userReducer.receipes);
    const favorites = useSelector(state => state.userReducer.favoritesReceipes);

    useEffect(() => {
        if (user) {
            dispatch(getUserReceipes(user._id));
            dispatch(getFavoritesReceipes(user._id));
        }
    }, [dispatch, user])


    return (
        <>
            <Tabs
                value={activeTab}
                indicatorColor="primary"
                textColor="primary"
                onChange={(e, newTab) => setActiveTab(newTab)}
                aria-label="disabled tabs example"
            >
                <Tab label="Les recettes"/>
                <Tab label="Les favoris"/>
            </Tabs>
            <TabPanel value={activeTab} index={0}>
                <Grid container spacing={4}>
                    {(receipes && receipes.length) ? receipes.map(receipe => {
                        return <Grid item xs={12} sm={6} key={receipe._id}><ReceipeItem receipe={receipe}/></Grid>
                    }) : (
                        <div>Pas de favoris</div>
                    )}
                </Grid>
            </TabPanel>
            <TabPanel value={activeTab} index={1}>
                <Grid container spacing={4}>
                    {(favorites && favorites.length) ? favorites.map(favorite => {
                        return <Grid item xs={12} sm={6} key={favorite._id}><ReceipeItem receipe={favorite}/></Grid>
                    }) : (
                        <div>Pas de favoris</div>
                    )}
                </Grid>
            </TabPanel>
        </>
    )
}

const TabPanel = (props) => {
    const {children, value, index, ...other} = props;

    return (
        <div
            role="tabpanel"
            {...other}
        >
            {value === index && (
                <Box p={3}>
                    <Typography>{children}</Typography>
                </Box>
            )}
        </div>
    );
}

export default ProfileTabs;