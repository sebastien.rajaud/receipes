import React, {useState} from "react";
import {getMediaUrl, uploadWithPreview} from "../../utils/Media";

const ImageToggle = ({toggle, component: Component, name, handleChange, mediaUrl, imageClasses, alternativeText, ...otherProps}) => {
    const [preview, setPreview] = useState(null);

    const handleInput = (e) => {
        uploadWithPreview(e,setPreview, handleChange)
    }

    return (
        <>
            {
                toggle ? (
                    <div style={{marginBottom: '15px'}}>
                        <img src={preview !== null ? preview : getMediaUrl(mediaUrl)}
                             style={{width: '100%'}}
                            alt=""
                        />
                        <input type="file" name={name} onChange={handleInput}/>
                    </div>
                ) : (

                    <Component
                        {...otherProps}
                        alternativeText={alternativeText}
                        mediaUrl={mediaUrl}
                        className={imageClasses}
                    />
                )
            }
        </>
    )
}


export default ImageToggle;