import Alert from "@material-ui/lab/Alert";
import React, {useState} from "react";
import {Snackbar} from "@material-ui/core";

const SnackAlert = ({message, severity, ...otherProps}) => {
    const [open, setOpen] = useState(true)

    return(
        <Snackbar open={open} autoHideDuration={6000} onClose={()=> { setOpen(state => false) }}
                  anchorOrigin={{ vertical:'bottom', horizontal:'center' }}>
            <Alert {...otherProps} severity={severity}>{message}</Alert>
        </Snackbar>
    )
}

export default SnackAlert;