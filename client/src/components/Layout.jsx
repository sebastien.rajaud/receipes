import Navbar from './Navbar/Navbar';
import {Container} from "@material-ui/core";

const Layout = ({children}) => {
    return (
        <div>
            <Navbar/>
            <Container>
                <main className="container-page">
                    {children}
                </main>
            </Container>
        </div>
    )
}

export default Layout;