import Form from './Form/Form';
import {CircularProgress, Grid, Paper, Typography} from '@material-ui/core';
import {useEffect} from "react";
import useStyles from './styles';
import {useSelector, useDispatch} from "react-redux";
import {getReceipes} from "../../actions/receipesActions";
import ReceipeItem from "./Receipe/ReceipeItem";

const Receipes = () => {
    const classes = useStyles();
    const isLoading = useSelector(state => state.receipeReducer.isLoading);
    const receipes = useSelector(state => state.receipeReducer.receipes);
    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(getReceipes())
    }, [dispatch])

    return (


        <Grid container spacing={5}>

            <Grid item xs={12} sm={8}>
                <Typography variant="h4" gutterBottom color="primary"> Receipes</Typography>
                {isLoading ? <CircularProgress /> : (
                    <Grid container spacing={4}>
                        {receipes && receipes.map(receipe => <Grid item xs={12} sm={6} md={6} key={receipe._id}>
                            <ReceipeItem
                                receipe={receipe}
                            />
                        </Grid>)}
                    </Grid>
                )}
            </Grid>

            <Grid item xs={12} sm={4}>
                <Paper className={classes.paper}>
                    <Typography variant="h4" gutterBottom>Ajouter vos recettes</Typography>
                    <Typography variant="body1" gutterBottom>Lorem ipsum dolor sit amet, consectetur adipisicing
                        elit. Consequuntur, deserunt eveniet ex fuga harum iste itaque molestiae numquam ratione
                        rem?</Typography>
                    <Form
                        oneColumn
                    />

                </Paper>
            </Grid>

        </Grid>


    )
}

export default Receipes