import {makeStyles} from "@material-ui/core";

export default makeStyles(theme => ({
    paper: {
        padding: theme.spacing(4)
    },
    list: {
        columnCount: 2,
        '& li': {
            display: 'flex',
            '&::before': {
                content: '""',
                display: 'block',
                width: '5px',
                height: '5px',
                borderRadius: '5px',
                background: theme.palette.grey[900],
                marginRight: '10px'
            }
        }
    },
    titleIngredients: {
        marginTop: theme.spacing(3)
    },
    divider: {
        marginTop: theme.spacing(1),
        marginBottom: theme.spacing(1)
    },
    stepsButtons: {
        marginTop: theme.spacing(2),
        '&:not(:last-child)': {
            marginRight: theme.spacing(2)
        }
    },
    timeInfos: {
        display: 'flex',
        justifyContent: 'space-between',
        alignItems: 'center',
        '& span': {
            display: 'flex',
            alignItems: 'center',
        }
    },
    avatar : {
        display: "inline-block",
        width: '30px',
        height: '30px',
        verticalAlign: 'middle',
        marginRight: theme.spacing(1)
    }
}))