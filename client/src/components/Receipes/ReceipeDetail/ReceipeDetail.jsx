import {useParams, useHistory} from 'react-router-dom';
import {useEffect, useState} from "react";
import {useDispatch, useSelector} from "react-redux";
import {getReceipeById} from '../../../actions/receipesActions';
import {
    Grid,
    Typography,
    List,
    ListItem,
    ListItemText,
    Divider,
    Paper,
    Stepper,
    Step,
    StepLabel,
    StepContent,
    Button,
    CircularProgress, Avatar
} from "@material-ui/core";
import Form from "../Form/Form";
import TimerIcon from "@material-ui/icons/Timer";
import EventIcon from '@material-ui/icons/Event';
import useStyles from './styles';
import {getMediaUrl} from "../../../utils/Media";
import EditBloc from "../Receipe/EditBloc";
import {deleteReceipe} from '../../../actions/receipesActions';
import {setFormValues} from "../../../actions/formReceipeActions";
import {renderDate} from "../../../utils/Date";
import FavoriteButton from "../Favorites/FavoriteButton";

const ReceipeDetail = () => {

    const {id} = useParams();
    const classes = useStyles();
    const dispatch = useDispatch();
    const history = useHistory();
    const receipe = useSelector(state => state.receipeReducer.selectedReceipe)
    const isLoading = useSelector(state => state.receipeReducer.isLoading);
    const user = useSelector(state => state.userReducer.user.user)
    const [activeStep, setActiveStep] = useState(0);


    useEffect(() => {
        if (id) {
            dispatch(getReceipeById(id))
        }
    }, [dispatch, id])

    const handleEdit = () => {
        dispatch(setFormValues(receipe))
        setActiveStep(0)
    }

    const handleDelete = (id) => {
        dispatch(deleteReceipe(id));
        history.push('/receipes');
    }


    return (
        <Grid container spacing={8}>

            <Grid item xs={12} sm={8}>
                { (isLoading || !id || !receipe) ? <CircularProgress/> : (
                    <Grid container spacing={5}>
                        <Grid item xs={12} sm={5}>
                            <div><img src={getMediaUrl(receipe.picture.path)} alt={receipe.title}/></div>
                            <EditBloc
                                user={user}
                                author={receipe.author}
                                handleEdit={handleEdit}
                                handleDelete={() => handleDelete(receipe._id)}
                            />
                            <div>
                                <FavoriteButton
                                    data={receipe}
                                    favorites={receipe.followers}
                                />
                            </div>
                        </Grid>
                        <Grid item xs={12} sm={7}>
                            <Typography variant="h4" color="primary" gutterBottom>{receipe.title}</Typography>
                            <Typography variant="subtitle1" color="textPrimary" gutterBottom>
                                <Avatar src={receipe.author.avatarUrl ? getMediaUrl(receipe.author.avatarUrl) : null}
                                        className={classes.avatar}/>By {receipe.author.firstName} {receipe.author.lastName}
                            </Typography>
                            <Divider className={classes.divider}/>
                            <div className={classes.timeInfos}>
                                <Typography variant="overline" color="textPrimary" gutterBottom><EventIcon
                                    color="secondary"/>&nbsp;{renderDate(receipe.createdAt)}</Typography>
                                <Typography variant="overline" color="textPrimary" gutterBottom><TimerIcon
                                    color="secondary" fontSize="small"/>&nbsp;{receipe.time}min.</Typography>
                            </div>
                            <Divider className={classes.divider}/>
                            <Typography variant="h5" color="secondary"
                                        className={classes.titleIngredients}>Ingrédients</Typography>
                            <List dense={true} disablePadding={false} className={classes.list}>
                                {receipe.ingredients.split(',').map((ingredient, index) => (
                                    <ListItem key={index}>
                                        <ListItemText
                                            primary={ingredient}
                                        />
                                    </ListItem>
                                ))}


                            </List>
                        </Grid>

                        <Grid item xs={12}>
                            <Divider className={classes.divider}/>
                            <Typography variant="h5" color="secondary"
                                        className={classes.titleIngredients}>Instructions</Typography>
                            <Stepper orientation="vertical" activeStep={activeStep} alternativelabel={false}>
                                {receipe.instructions.split(/\n/).filter(Boolean).map((step, index) => (
                                    <Step key={index} completed={index < activeStep ? true : false}>
                                        <StepLabel><Typography
                                            variant="subtitle1">Etape {index + 1}</Typography></StepLabel>
                                        <StepContent>
                                            <Typography variant="body1">{step}</Typography>
                                        </StepContent>

                                    </Step>
                                ))}
                                <div>
                                    <Button disabled={activeStep === 0} size="small" variant="outlined" color="primary"
                                            className={classes.stepsButtons}
                                            onClick={() => setActiveStep(state => state - 1)}>-</Button>
                                    <Button
                                        disabled={activeStep === receipe.instructions.split(/\n/).filter(Boolean).length - 1}
                                        size="small" variant="outlined" color="primary" className={classes.stepsButtons}
                                        onClick={() => setActiveStep(state => state + 1)}>+</Button>
                                </div>
                            </Stepper>
                        </Grid>
                    </Grid>
                )}
            </Grid>

            <Grid item xs={6} sm={4}>
                <Paper className={classes.paper}>
                    <Form
                        oneColumn
                    />
                </Paper>
            </Grid>

        </Grid>
    )
}

export default ReceipeDetail;