import {makeStyles} from "@material-ui/core";


export default makeStyles(theme => ({
    paper: {
        padding: theme.spacing(3)
    },
    toggleButton : {
        marginTop: theme.spacing(2),
        marginBottom: theme.spacing(2)
    }
}))