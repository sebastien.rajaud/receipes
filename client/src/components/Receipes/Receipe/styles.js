import {makeStyles} from "@material-ui/core";

export default makeStyles(theme => ({
    root: {
        maxWidth: 345,
    },
    media: {
        height: 0,
        paddingTop: '56.25%', // 16:9
    },
    cardActions: {
      justifyContent: 'space-between'
    },
    avatar: {
        backgroundColor: 'red',
    },
    icons: {
        display: "flex",
        justifyContent: 'space-between',
        alignItems: 'center',
        marginBottom: theme.spacing(2)
    },
    divider: {
        marginBottom: theme.spacing(2)
    },
    timer: {
        display: "flex",
        justifyContent: 'justify-content',
        alignItems: 'center',
    }
}))