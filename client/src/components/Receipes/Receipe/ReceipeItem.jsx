import {
    CardMedia,
    CardHeader,
    CardContent,
    CardActions,
    Card,
    Typography,
    Button,
    Divider
} from "@material-ui/core";
import TimerIcon from '@material-ui/icons/Timer';
import Avatar from '@material-ui/core/Avatar';
import useStyles from './styles';
import {getMediaUrl} from "../../../utils/Media";
import {useSelector, useDispatch} from "react-redux";
import {setFormValues} from "../../../actions/formReceipeActions";
import {deleteReceipe} from "../../../actions/receipesActions";
import {Link} from 'react-router-dom';
import {renderDate} from '../../../utils/Date';
import EditBloc from './EditBloc';
import FavoriteButton from "../Favorites/FavoriteButton";

const ReceipeItem = ({receipe}) => {

    const user = useSelector(state => state.userReducer.user.user);

    const {author} = receipe;
    const dispatch = useDispatch();
    const classes = useStyles();

    const handleEdit = (receipe) => {
        dispatch(setFormValues(receipe))
    }

    const handleDelete = (id) => {
        dispatch(deleteReceipe(id))
    }


    return (
        <Card>
            <CardHeader
                avatar={
                    <Avatar
                        aria-label="receipe"
                        className={classes.avatar}
                        src={receipe.author.avatarUrl ? getMediaUrl(receipe.author.avatarUrl) : null}
                        component={Link}
                        to={`/user/profile/${receipe.author._id}`}
                    >
                        R
                    </Avatar>
                }
                titleTypographyProps={{
                    variant: 'h6',
                    color: 'primary'
                }}
                title={receipe.title}
                subheader={`${renderDate(receipe.createdAt)}`}
            />
            <CardMedia
                className={classes.media}
                image={receipe.picture ? getMediaUrl(receipe.picture.path) : ''}
                title={receipe.title}
            />
            <CardContent>
                <div className={classes.icons}>

                    <EditBloc user={user}
                              author={author}
                              handleEdit={() => handleEdit(receipe)}
                              handleDelete={() => handleDelete(receipe._id)}
                    />
                    <Typography variant="overline" color="textSecondary" component="div" className={classes.timer}>
                        <TimerIcon color="secondary" fontSize="small"/>&nbsp;{receipe.time}min.
                    </Typography>
                </div>
                <Divider className={classes.divider}/>
                <Typography variant="body2" color="textSecondary" component="p">
                    {receipe.instructions.slice(0, 80)}[...]
                </Typography>
            </CardContent>
            <CardActions disableSpacing className={classes.cardActions}>
                <div>
                    <FavoriteButton
                        data={receipe}
                        favorites={receipe.followers}
                    />

                </div>

                <Button variant="text" component={Link} to={`/receipes/${receipe._id}`}>En savoir plus</Button>
            </CardActions>


        </Card>
    )
}
export default ReceipeItem