import {IconButton} from "@material-ui/core";
import EditIcon from "@material-ui/icons/Edit";
import DeleteIcon from "@material-ui/icons/Delete";

const EditBloc = ({user, author, handleEdit, handleDelete}) => {

    return (
        <>
            {user && user._id === author._id && (
                <div>
                    <IconButton size="small" onClick={handleEdit}>
                        <EditIcon fontSize="small"/>
                    </IconButton>
                    <IconButton size="small" onClick={handleDelete}>
                        <DeleteIcon fontSize="small"/>
                    </IconButton>
                </div>
            )}
        </>
    )
}

export default EditBloc;