import React, {useEffect, useState} from "react";
import {IconButton, Typography, Icon} from "@material-ui/core";
import FavoriteIcon from "@material-ui/icons/Favorite";
import FavoriteBorderIcon from "@material-ui/icons/FavoriteBorder";
import {useDispatch, useSelector} from "react-redux";
import useStyles from './styles';
import {addReceipeToFavorite, removeReceipeToFavorite} from '../../../actions/receipesActions';

const FavoriteButton = ({data, favorites}) => {

    const user = useSelector(state => state.userReducer.user.user);
    const dispatch = useDispatch();

    const classes = useStyles();

    const [isAdded, setIsAdded] = useState(false);


    useEffect(() => {
        if (user) {
            setIsAdded(favorites.some(el => el.toString() === user._id.toString()))
        }
    }, [favorites, user])

    if (!user) {
        return false;
    }

    const handleClick = () => {
        if (isAdded) {

            dispatch(removeReceipeToFavorite(data._id))
        } else {
            dispatch(addReceipeToFavorite(data._id))

        }
    }

    if (!user) {
        return false
    }

    return (
        <>
            {user._id === data.author._id ? (
                <div className={classes.favoriteContainer}>
                    <Icon color="inherit">
                        <FavoriteIcon/>
                    </Icon>&nbsp;<Typography variant="overline"
                                             color="textSecondary">{`${favorites.length} ${favorites.length > 1 ? 'followers' : 'follower'}`}</Typography>
                </div>
            ) : (
                <div className={classes.favoriteContainer}>
                    <IconButton aria-label="add to favorites" onClick={handleClick}>
                        {isAdded ? <FavoriteIcon/> : <FavoriteBorderIcon/>}
                    </IconButton>&nbsp;<Typography variant="overline"
                                                   color="textSecondary">{`${favorites.length} ${favorites.length > 1 ? 'followers' : 'follower'}`}</Typography>
                </div>
            )}
        </>
    )
}

export default FavoriteButton;