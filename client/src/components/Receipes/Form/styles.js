import {makeStyles} from "@material-ui/core";


export default makeStyles(theme => ({
    buttons: {
        margin: `0 ${theme.spacing(1)}px`
    }
}))