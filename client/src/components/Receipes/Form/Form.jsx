import React, {useEffect, useState} from "react";
import {Button, TextField, Grid} from '@material-ui/core';
import {getMediaUrl, uploadWithPreview} from "../../../utils/Media";
import useStyles from "./styles";
import {useDispatch, useSelector} from "react-redux";
import {addReceipe, updateReceipe} from "../../../actions/receipesActions";
import {resetFormValues} from '../../../actions/formReceipeActions';
import SnackAlert from "../../Errors/SnackAlert";

const Form = ({oneColumn}) => {
    const initialFormData = {
        title: '',
        ingredients: '',
        instructions: '',
        time: '',
        picture: ''
    }
    const editMode = useSelector(state => state.receipeFormReducer.editMode)
    const formValues = useSelector(state => state.receipeFormReducer.values)
    const user = useSelector(state => state.userReducer.user.user)
    const message = useSelector(state => state.receipeReducer.message)

    const [formData, setFormData] = useState(initialFormData);
    const [preview, setPreview] = useState(editMode ? getMediaUrl(formValues.picture.path) : null);

    const classes = useStyles();
    const dispatch = useDispatch();

    useEffect(() => {
        if (formValues) {
            setFormData(formValues);
            setPreview(state => editMode ? getMediaUrl(formValues.picture.path) : null)
        }

    }, [formValues, editMode]);


    const handleChange = (e) => {
        setFormData(state => ({
            ...state,
            [e.target.name]: e.target.value
        }))
    }

    const handleChangePicture = (e) => {
        uploadWithPreview(e, setPreview, (picture) => setFormData(state => ({
            ...state,
            picture: picture
        })))
    }

    const handleSubmit = (e) => {
        e.preventDefault();
        if (editMode) {
            dispatch(updateReceipe(formData, formData._id))
        } else {
            dispatch(addReceipe({...formData, author: user._id}))
        }
        clearForm();
    }

    const clearForm = () => {
        setFormData(initialFormData);
        dispatch(resetFormValues())
    }

    const handleFormReset = () => {
        clearForm();
    }

    return (
        <form onSubmit={handleSubmit}>
            <Grid container spacing={3} justify="space-between">
                <Grid item xs={12} sm={oneColumn ? 12 : 4}>

                    <div style={{marginBottom: '15px'}}>
                        <img src={preview ? preview : ''}
                             style={{width: '100%'}}
                            alt=""
                        />
                        <TextField
                            variant="outlined"
                            name="picture"
                            type="file"
                            onChange={handleChangePicture}
                        />
                    </div>
                </Grid>
                <Grid item xs={12} sm={oneColumn ? 12 : 8}>

                    <TextField
                        variant="outlined"
                        label="Titre"
                        name="title"
                        value={formData.title}
                        placeholder="Pâtes à la carbonara"
                        required
                        fullWidth
                        onChange={handleChange}

                    />
                    <TextField
                        variant="outlined"
                        label="Temps de préparation"
                        name="time"
                        value={formData.time}
                        type="number"
                        placeholder="12"
                        required
                        fullWidth
                        onChange={handleChange}
                        margin="normal"
                    />
                </Grid>
                <Grid item xs={12} sm={oneColumn ? 12 : 4}>
                    <TextField
                        variant="outlined"
                        label="Ingrédients"
                        name="ingredients"
                        value={formData.ingredients}
                        multiline={true}
                        placeholder="Pâtes, oeufs, persil ... "
                        helperText="Séparez les ingrédients par une virgule"
                        rows={12}
                        fullWidth
                        onChange={handleChange}
                    />
                </Grid>
                <Grid item xs={12} sm={oneColumn ? 12 : 8}>
                    <TextField
                        variant="outlined"
                        label="Instructions"
                        name="instructions"
                        value={formData.instructions}
                        multiline={true}
                        placeholder="Faites bouillir de l'eau "
                        helperText="Séparez les étapes par un retour à la ligne"
                        rows={12}
                        fullWidth
                        onChange={handleChange}
                    />
                </Grid>

                <Grid container justify="flex-end" spacing={0}>
                    <Button className={classes.buttons} type="submit" variant="contained" color="primary">
                        {editMode ? 'Modifier la recette' : 'Ajouter ma recette'}
                    </Button>
                    <Button className={classes.buttons}
                            onClick={handleFormReset}
                            type="reset" variant="outlined" color="secondary">Reset</Button>
                </Grid>
                {message.active && <SnackAlert severity={message.type} message={message.message} />}

            </Grid>

        </form>

    )
}

export default Form;