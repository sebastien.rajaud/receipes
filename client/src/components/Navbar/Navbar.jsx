import React, {useState} from 'react';
import {AppBar, Container, Toolbar, Typography, Button, IconButton, Menu, MenuItem} from "@material-ui/core";
import MenuIcon from '@material-ui/icons/Menu';
import useStyles from './styles';
import {useSelector, useDispatch} from "react-redux";
import ExitToAppIcon from '@material-ui/icons/ExitToApp';
import {Link} from 'react-router-dom';
import {logout} from "../../actions/usersActions";
import CustomAvatar from "../Profile/CustomAvatar";
import SidebarMenu from './SidebarMenu/SidebarMenu';
import SnackAlert from "../Errors/SnackAlert";

const Navbar = () => {
    const classes = useStyles();
    const user = useSelector(state => state.userReducer.user.user);
    const isAuthenticated = useSelector(state => state.userReducer.user.isAuthenticated)
    const dispatch = useDispatch();
    const [anchorEl, setAnchorEl] = useState(null)
    const [openSidebar, setOpenSidebar] = useState(false)
    const message = useSelector(state => state.userReducer.message)

    const handleLogout = () => {
        dispatch(logout());
    }

    const handleMenu = (e) => {
        setAnchorEl(e.currentTarget);
    }

    const toggleSidebar = () => {
        setOpenSidebar(state => !state)
    }

    return (
        <>
            <AppBar position="static">
                <SidebarMenu anchor="left" open={openSidebar} handleClose={() => setOpenSidebar(false)}/>
                <Container>
                    <Toolbar>
                        <IconButton
                            onClick={toggleSidebar}
                            edge="start" className={classes.menuButton} color="inherit" aria-label="menu">
                            <MenuIcon/>
                        </IconButton>
                        <Typography variant="h6" className={classes.title}>
                            <Button component={Link} to="/" variant="text" color="inherit">Receipes</Button>
                        </Typography>
                        {isAuthenticated ? (
                            <>
                                <IconButton onClick={handleMenu}>
                                    <CustomAvatar mediaUrl={user.avatar ? user.avatar.path : null}
                                                  alternativeText={`${user.firstName} ${user.lastName}`}/>
                                    <Typography variant="body2"
                                                className={classes.userName}>{user.firstName} {user.lastName}</Typography>
                                </IconButton>
                                <Menu
                                    id="menu-appbar"
                                    anchorOrigin={{
                                        vertical: 'top',
                                        horizontal: 'left',
                                    }}
                                    anchorEl={anchorEl}
                                    keepMounted
                                    transformOrigin={{
                                        vertical: 'top',
                                        horizontal: 'right',
                                    }}
                                    open={Boolean(anchorEl)}
                                    onClose={() => setAnchorEl(null)}
                                >
                                    <MenuItem
                                        to={`/user/profile/${user._id}`}
                                        component={Link}
                                    >Profil</MenuItem>
                                    <MenuItem
                                        to={`/user/edit/${user._id}`}
                                        component={Link}
                                    >Modification identifiants</MenuItem>
                                    <MenuItem onClick={handleLogout}><ExitToAppIcon/>&nbsp;Logout</MenuItem>
                                </Menu>
                            </>
                        ) : <Button component={Link} to={'/auth'} color="inherit">Login</Button>
                        }

                    </Toolbar>
                </Container>
            </AppBar>
            {message.active && <SnackAlert severity={message.type} message={message.message}/>}
        </>
    )
}

export default Navbar;