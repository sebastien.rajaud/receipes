import {makeStyles} from "@material-ui/core";

export default makeStyles(theme => ({
    sidebar: {
        width: '250px',
    },
    sidebarTitle: {
        padding: theme.spacing(2)
    },
    listItemText: {
        marginLeft: theme.spacing(2)
    }
}))