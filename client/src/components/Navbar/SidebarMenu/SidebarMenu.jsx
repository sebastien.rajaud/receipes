import Drawer from '@material-ui/core/Drawer';
import {Divider, List, ListItem, ListItemText, Typography} from "@material-ui/core";
import useStyles from './styles';
import RestaurantIcon from '@material-ui/icons/Restaurant';
import {Link} from 'react-router-dom';
import ChatBubbleIcon from '@material-ui/icons/ChatBubble';

const SidebarMenu = ({anchor, open, handleClose}) => {
    const classes = useStyles();

    return (
        <Drawer
            anchor={anchor} open={open} onClose={handleClose}>
            <div className={classes.sidebar}>
                <Typography variant="h4" className={classes.sidebarTitle} color="primary">Receipes</Typography>

                <List>
                    <ListItem component={Link} to="/receipes" onClick={handleClose}>
                        <RestaurantIcon/>
                        <ListItemText className={classes.listItemText} primary="Les recettes"/>
                    </ListItem>
                    <Divider/>
                    <ListItem component={Link} to="/tchat" onClick={handleClose}>
                        <ChatBubbleIcon/>
                        <ListItemText className={classes.listItemText} primary="Le tchat"/>
                    </ListItem>
                </List>
            </div>
        </Drawer>
    )
}

export default SidebarMenu;