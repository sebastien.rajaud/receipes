import {makeStyles} from "@material-ui/core";

export default makeStyles(theme => ({
    tchatContainer: {
        height: '500px',
        alignItems: 'stretch',
        marginTop: theme.spacing(5),

    },
    usersColumm: {
        height: '100%'
    },
    messagesContainer: {
        height: '400px',
        overflowY: 'auto'
    },
    paper: {
        padding: theme.spacing(3)
    },
    textarea: {
        marginBottom: theme.spacing(2)
    },
    form: {
        marginTop: theme.spacing(2)
    }
}))