import {Typography} from "@material-ui/core";
import useStyles from './styles';

const Message = ({message, author, fromUser}) => {
    const classes = useStyles();

    return (
        <>

            <Typography
                variant="overline">{fromUser ? 'Vous' : author} :</Typography>
            <Typography
                variant="body2" className={`${classes.message} ${fromUser && classes.fromUser} `}>{message}</Typography>
        </>
    )
}

export default Message;