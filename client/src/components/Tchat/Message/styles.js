import {makeStyles} from "@material-ui/core";

export default makeStyles(theme => ({
    message : {
        background: theme.palette.primary.main,
        color: theme.palette.common.white,
        marginBottom: theme.spacing(2),
        padding: theme.spacing(1),
        borderRadius: theme.shape.borderRadius
    },
    fromUser : {
        marginRight: 0,
         background: theme.palette.secondary.main,
    }
}))
