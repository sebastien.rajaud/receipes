import {makeStyles} from "@material-ui/core";

export default makeStyles(theme => ({

    userContainer: {
        display: 'flex',
        alignItems: 'center',
        marginBottom: theme.spacing(2)
    },
    avatar: {
        width: 30,
        height: 30,
        marginRight: theme.spacing(1)
    }
}))