import { Typography} from "@material-ui/core";
import CustomAvatar from "../../Profile/CustomAvatar";
import React from "react";
import useStyles from './styles';

const User = ({firstName}) => {
const classes = useStyles();
    return (
        <div className={classes.userContainer}>
            <CustomAvatar className={classes.avatar}/>
            <Typography variant="body2">{firstName}</Typography>
        </div>
    )
}

export default User;