import React, {useEffect, useRef, useState} from 'react';
import {Grid, Typography, TextField, Button, Paper} from "@material-ui/core";
import useStyles from './styles';
import Message from "./Message/Message";
import User from "./User/User";
import {useDispatch, useSelector} from "react-redux";
import io from 'socket.io-client';
import {initHistory, initTchat, changeRoom, updateMessage, changeNamespace} from "../../actions/tchatActions";


const Tchat = () => {
    const classes = useStyles();
    const [formData, setFormData] = useState('');
    const endChatBox = useRef(null);

    const dispatch = useDispatch();
    const user = useSelector(state => state.userReducer.user.user);
    const namespaces = useSelector(state => state.tchatReducer.namespaces);
    const rooms = useSelector(state => state.tchatReducer.rooms);
    const activeRoom = useSelector(state => state.tchatReducer.activeRoom);

    const messages = useSelector(state => state.tchatReducer.messages);

    const activeNsSocket = useRef(null);
    const allRooms = useRef([]);
    const [namespaceSockets, setNamespaceSockets] = useState([]);

    const ioClient = io(process.env.REACT_APP_WS_DOMMAIN, {
        withCredentials: true
    });

    useEffect(() => {
        let activeRoom;
        let messages = [];
        let namespaceRooms = [];
        let init = false;

        try {

            ioClient.on('connect', (socket) => {
                console.log(' Connexion client OK')

            })

            ioClient.on('namespaces', data => {
                let namespaces;
                let socketArray = [];


                namespaces = data;
                allRooms.current = [];

                for (let ns of namespaces) {
                    const nsSocket = io(`${process.env.REACT_APP_WS_DOMMAIN}/${ns._id}`);
                    nsSocket.on('rooms', data => {

                        allRooms.current.push(...data);
                        if (!init) {
                            init = true;
                            activeNsSocket.current = nsSocket;
                            activeRoom = allRooms.current.find(room => `/${room.namespace}` === activeNsSocket.current.nsp && room.index === 0)
                            activateRoom(activeRoom)
                        }

                        namespaceRooms = filterRoomsByNamespace(allRooms.current);

                        if (`/${ns._id}` === activeNsSocket.current.nsp) {
                            dispatch(initTchat(namespaces, namespaceRooms, messages, activeRoom))
                        }

                        socketArray.push(nsSocket);
                        setNamespaceSockets(socketArray)

                    })

                    nsSocket.on('history', messages => {
                        dispatch(initHistory(messages));
                    })

                    nsSocket.on('message', message => {
                        dispatch(updateMessage(message))
                    })
                }
            })


        } catch (error) {
            throw error
        }

    }, [dispatch]);

    useEffect(() => {
        scrollChatBox()
    }, [messages])


    const filterRoomsByNamespace = (rooms) => {
        return rooms.filter(room => `/${room.namespace}` === activeNsSocket.current.nsp)
    }

    const activateRoom = (room) => {
        activeNsSocket.current.emit('joinRoom', room._id)
    }


    const handleChangeNamespace = (namespace) => {

        const newNamespaceSocket = namespaceSockets.find(nss => `/${namespace._id}` === nss.nsp)

        if (activeNsSocket.current.nsp !== `/${namespace._id}`) {
            activeNsSocket.current.emit('leaveRoom', activeRoom._id)
        }
        activeNsSocket.current = newNamespaceSocket;
        let rooms = filterRoomsByNamespace(allRooms.current);
        activateRoom(rooms[0]);
        dispatch(changeNamespace(rooms))
    }

    const handleChangeRoom = (room) => {
        dispatch(changeRoom(room))
        activeNsSocket.current.emit('leaveRoom', activeRoom._id)
        activeNsSocket.current.emit('joinRoom', room._id)
    }
    const handleChangeForm = (e) => {
        setFormData(e.target.value)
    }

    const scrollChatBox = () => {
        endChatBox.current.scrollIntoView({behavior: "smooth"});
    }

    const handleSubmitMessage = (e) => {
        e.preventDefault();
        activeNsSocket.current.emit('message', formData, activeRoom._id, user)
        setFormData('');
    }

    return (
        <>
            <Typography
                variant="h4"
                gutterBottom
            >Tchat</Typography>
            <Typography variant="body1" gutterBottom>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cumque
                exercitationem
                nam unde. Aut corporis dolorem expedita id, laborum magnam molestiae mollitia quia quibusdam voluptate!
                Aliquid delectus dicta exercitationem iure possimus.</Typography>
            <Grid container spacing={1} className={classes.tchatContainer} alignItems="stretch">
                <Grid item xs={12} sm={3} md={2} className={classes.usersColumm}>
                    <Paper className={classes.paper}>
                        <Typography variant="h6" color="secondary" gutterBottom>Channel</Typography>
                        {
                            namespaces && namespaces.map(namespace => <Typography
                                variant="body2"
                                key={namespace._id}
                                onClick={() => handleChangeNamespace(namespace)}
                                color={activeNsSocket.current.nsp === `/${namespace._id}` ? 'primary' : 'textPrimary'}
                            >#{namespace.title}</Typography>)
                        }
                    </Paper>
                </Grid>

                <Grid item xs={12} sm={3} md={2} className={classes.usersColumm}>
                    <Paper className={classes.paper}>
                        <Typography variant="h6" color="secondary" gutterBottom>Rooms</Typography>
                        {
                            rooms && rooms.map(room => <Typography
                                onClick={() => handleChangeRoom(room)}
                                variant="body2"
                                key={room._id}
                                color={activeRoom._id === room._id ? 'primary' : 'textPrimary'}
                            >#{room.title}</Typography>)
                        }
                    </Paper>
                </Grid>
                <Grid item xs={12} sm={6} md={6}>
                    <Paper className={`${classes.paper} ${classes.messagesContainer}`} id="chatbox">

                        {
                            messages.map(message => <Message
                                key={message._id}
                                message={message.data}
                                author={message.authorName}
                                fromUser={message.author === user._id}
                            />)
                        }
                        <div ref={endChatBox}></div>
                    </Paper>

                    <Paper className={`${classes.paper} ${classes.form}`}>
                        <Typography variant="h5" color="secondary" gutterBottom>Votre message</Typography>
                        <form onSubmit={handleSubmitMessage}>
                            <TextField fullWidth value={formData} onChange={handleChangeForm} type="textarea"
                                       variant="outlined" className={classes.textarea}/>
                            <Button type="submit" variant="contained" color="primary">
                                OK
                            </Button>
                        </form>
                    </Paper>
                </Grid>
                <Grid item xs={12} sm={3} md={2} className={classes.usersColumm}>
                    <Paper className={classes.paper}>
                        <Typography variant="h6" color="secondary" gutterBottom>Users</Typography>
                        <User firstName="Jean"/>
                        <User firstName="Pierre"/>
                        <User firstName="Julie"/>
                        <User firstName="Françoise"/>
                    </Paper>
                </Grid>
            </Grid>
        </>
    )
}

export default Tchat;