export const getMediaUrl = (path) => {
    const arrayUrl = path.split('/')
    if (arrayUrl[0] === 'public') {
        arrayUrl.shift();
    }
    const mediaUrl = arrayUrl.join('/');
    return `http://localhost:5000/${mediaUrl}`;
}

export const uploadWithPreview = (e, setPreview, handleChange) => {
    const file = e.target.files[0] || e.dataTransfer.files[0]
    setPreview('')
    if (file) {
        const fileReader = new FileReader();

        fileReader.onload = () => {
            setPreview(fileReader.result)
        }
        fileReader.readAsDataURL(file)

        handleChange(file);
    }
}