import {
    INIT_TCHAT,
    INIT_TCHAT_ERROR,
    INIT_TCHAT_SUCCESS,
    UPDATE_MESSAGE,
    UPDATE_MESSAGE_SUCCESS,
    UPDATE_MESSAGE_ERROR,
    INIT_HISTORY,
    INIT_HISTORY_SUCCESS,
    INIT_HISTORY_ERROR,
    CHANGE_ROOM_SUCCESS,
    CHANGE_NAMESPACE_SUCCESS
} from "./constants";



export const initTchat = (namespaces, rooms, messages, activeRoom) => async dispatch => {
    dispatch({
        type: INIT_TCHAT
    })
    try {
        dispatch({
            type: INIT_TCHAT_SUCCESS,
            namespaces: namespaces,
            rooms: rooms,
            messages: messages,
            activeRoom: activeRoom
        })


    } catch (e) {
        dispatch({
            type: INIT_TCHAT_ERROR,
            error: e.message
        })
    }
}


export const updateMessage = (data) => async dispatch => {
    dispatch({
        type: UPDATE_MESSAGE
    })
    try {
        dispatch({
            type: UPDATE_MESSAGE_SUCCESS,
            data
        })
    } catch (error) {
        dispatch({
            type: UPDATE_MESSAGE_ERROR,
            error: error.message
        })
    }
}

export const initHistory = (data) => async dispatch => {
    dispatch({
        type: INIT_HISTORY
    })
    try {
        dispatch({
            type: INIT_HISTORY_SUCCESS,
            data
        })
    } catch (error) {
        dispatch({
            type: INIT_HISTORY_ERROR,
            error: error.message
        })
    }
}

export const changeRoom = (data) => dispatch => {

    dispatch({
        type: CHANGE_ROOM_SUCCESS,
        data
    })

}

export const changeNamespace = (data) => dispatch => {

    dispatch({
        type: CHANGE_NAMESPACE_SUCCESS,
        data
    })

}