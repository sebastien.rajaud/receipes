import {
    ADD_RECEIPE,
    ADD_RECEIPE_ERROR,
    ADD_RECEIPE_SUCCESS,
    ADD_RECEIPE_TO_FAVORITE,
    ADD_RECEIPE_TO_FAVORITE_SUCCESS,
    ADD_RECEIPE_TO_FAVORITE_ERROR,
    REMOVE_RECEIPE_TO_FAVORITE,
    REMOVE_RECEIPE_TO_FAVORITE_SUCCESS,
    REMOVE_RECEIPE_TO_FAVORITE_ERROR,
    DELETE_RECEIPE,
    DELETE_RECEIPE_ERROR,
    DELETE_RECEIPE_SUCCESS,
    GET_RECEIPES,
    GET_RECEIPES_ERROR,
    GET_RECEIPES_SUCCESS,
    GET_SELECTED_RECEIPE,
    GET_SELECTED_RECEIPE_ERROR, GET_SELECTED_RECEIPE_SUCCESS,
    UPDATE_RECEIPE,
    UPDATE_RECEIPE_ERROR,
    UPDATE_RECEIPE_SUCCESS,
    GET_USER_RECEIPES,
    GET_USER_RECEIPES_ERROR,
    GET_USER_RECEIPES_SUCCESS,
    GET_FAVORITES_RECEIPES,
    GET_FAVORITES_RECEIPES_ERROR,
    GET_FAVORITES_RECEIPES_SUCCESS
} from "./constants";
import * as API from '../api/index';

export const getReceipes = () => async dispatch => {
    dispatch({
        type: GET_RECEIPES
    })
    try {
        const {data} = await API.getReceipes();
        dispatch({
            type: GET_RECEIPES_SUCCESS,
            data
        })

    } catch (error) {
        dispatch({
            type: GET_RECEIPES_ERROR,
            error: error?.response?.data
        })
    }
}

export const addReceipe = (formData) => async dispatch => {
    dispatch({
        type: ADD_RECEIPE
    })

    try {
        const {data} = await API.addReceipe(formData);
        dispatch({
            type: ADD_RECEIPE_SUCCESS,
            data,

        })
    } catch (error) {
        dispatch({
            type: ADD_RECEIPE_ERROR,
            error: error?.response?.data
        })
    }
}

export const updateReceipe = (formData, id) => async dispatch => {
    dispatch({
        type: UPDATE_RECEIPE
    })

    try {
        const {data} = await API.updateReceipe(formData, id);
        dispatch({
            type: UPDATE_RECEIPE_SUCCESS,
            data,
        })
    } catch (error) {
        dispatch({
            type: UPDATE_RECEIPE_ERROR,
            error: error?.response?.data
        })
    }
}

export const deleteReceipe = (id) => async dispatch => {
    dispatch({
        type: DELETE_RECEIPE
    })
    try {
        const {data} = await API.deleteReceipe(id);

        dispatch({
            type: DELETE_RECEIPE_SUCCESS,
            data
        })
    } catch (error) {
        dispatch({
            type: DELETE_RECEIPE_ERROR,
            error: error?.response?.data
        })
    }
}

export const getReceipeById = (id) => async dispatch => {

    dispatch({
        type: GET_SELECTED_RECEIPE
    })
    try {
        const {data} = await API.getSelectedReceipe(id);
        dispatch({
            type: GET_SELECTED_RECEIPE_SUCCESS,
            data
        })

    } catch (error) {
        dispatch({
            type: GET_SELECTED_RECEIPE_ERROR,
            error: error?.response?.data
        })
    }
}

export const addReceipeToFavorite = (dataId) => async dispatch => {
    dispatch({
        type: ADD_RECEIPE_TO_FAVORITE
    })
    try {
        const {data} = await API.addReceipeToFavorite(dataId);
        dispatch({
            type: ADD_RECEIPE_TO_FAVORITE_SUCCESS,
            data
        })
    } catch (error) {
        dispatch({
            type: ADD_RECEIPE_TO_FAVORITE_ERROR,
            error: error?.response.data
        })
    }

}


export const removeReceipeToFavorite = (dataId) => async dispatch => {
    dispatch({
        type: REMOVE_RECEIPE_TO_FAVORITE
    })
    try {
        const {data} = await API.removeReceipeToFavorite(dataId);
        dispatch({
            type: REMOVE_RECEIPE_TO_FAVORITE_SUCCESS,
            data
        })
    } catch (error) {
        dispatch({
            type: REMOVE_RECEIPE_TO_FAVORITE_ERROR,
            error: error?.response.data
        })
    }

}

export const getUserReceipes = (userId) => async  dispatch => {
    dispatch({
        type: GET_USER_RECEIPES
    })
    try {
        const {data} = await API.getUserReceipes(userId);
        dispatch({
            type: GET_USER_RECEIPES_SUCCESS,
            data
        })
    } catch (error) {
        dispatch({
            type: GET_USER_RECEIPES_ERROR,
            error: error?.response?.data
        })
    }

}


export const getFavoritesReceipes = (userId) => async  dispatch => {
    dispatch({
        type: GET_FAVORITES_RECEIPES
    })
    try {
        const {data} = await API.getFavoritesReceipes(userId);
        dispatch({
            type: GET_FAVORITES_RECEIPES_SUCCESS,
            data
        })
    } catch (error) {
        dispatch({
            type: GET_FAVORITES_RECEIPES_ERROR,
            error: error?.response?.data
        })
    }

}