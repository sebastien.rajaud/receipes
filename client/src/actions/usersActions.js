import {
    EDIT_USER_PROFILE,
    EDIT_USER_PROFILE_ERROR,
    EDIT_USER_PROFILE_SUCCESS,
    GET_USER_PROFILE,
    GET_USER_PROFILE_ERROR,
    GET_USER_PROFILE_SUCCESS,
    INIT_SESSION,
    INIT_SESSION_ERROR,
    INIT_SESSION_SUCCESS,
    LOGOUT,
    LOGOUT_ERROR,
    LOGOUT_SUCCESS, RESET_PASSWORD, RESET_PASSWORD_ERROR, RESET_PASSWORD_SUCCESS,
    SEND_EMAIL_FOR_RESET_PASSWORD,
    SEND_EMAIL_FOR_RESET_PASSWORD_ERROR,
    SEND_EMAIL_FOR_RESET_PASSWORD_SUCCESS,
    SIGNIN,
    SIGNIN_ERROR,
    SIGNIN_SUCCESS,
    SIGNUP,
    SIGNUP_ERROR,
    SIGNUP_SUCCESS
} from "./constants";
import * as API from '../api';

export const initSession = () => async dispatch => {
    dispatch({
        type: INIT_SESSION
    })
    try {
        const {data} = await API.initSession();
        dispatch({
            type: INIT_SESSION_SUCCESS,
            data
        })
    } catch (error) {
        dispatch({
            type: INIT_SESSION_ERROR,
            error: error?.response?.message
        })
    }

}
export const signin = (formData, history) => async dispatch => {

    dispatch({
        type: SIGNIN,
    });

    try {
        const {data} = await API.signin(formData);
        dispatch({
            type: SIGNIN_SUCCESS,
            data
        })
        history.push(`/user/profile/${data.user._id}`)
    } catch (error) {
        dispatch({
            type: SIGNIN_ERROR,
            error: error?.response?.data
        })
    }
}
export const signup = (formData, history) => async dispatch => {

    dispatch({
        type: SIGNUP
    })
    try {
        const {data} = await API.signup(formData);
        dispatch({
            type: SIGNUP_SUCCESS,
            data
        })
        history.push(`/user/profile/${data.user._id}`)

    } catch (error) {

        dispatch({
            type: SIGNUP_ERROR,
            error: error.response.data
        })
    }
}
export const logout = () => async dispatch => {
    dispatch({
        type: LOGOUT
    })
    try {
        const {data} = await API.logout();
        dispatch({
            type: LOGOUT_SUCCESS,
            data
        })
    } catch (error) {
        dispatch({
            type: LOGOUT_ERROR,
            error: error.response.data
        })
    }

}
export const getUserProfile = (id) => async dispatch => {
    dispatch({
        type: GET_USER_PROFILE
    })
    try {
        const {data} = await API.getUserProfile(id);
        dispatch({
            type: GET_USER_PROFILE_SUCCESS,
            data
        })
    } catch (error) {
        dispatch({
            type: GET_USER_PROFILE_ERROR,
            error: error?.response?.data
        })
    }


}
export const editUserProfile = (id, formData) => async dispatch => {
    dispatch({
        type: EDIT_USER_PROFILE
    })
    try {
        const {data} = await API.editUserProfile(id, formData)
        dispatch({
            type: EDIT_USER_PROFILE_SUCCESS,
            data
        })
    } catch (error) {
        dispatch({
            type: EDIT_USER_PROFILE_ERROR,
            error: error.response?.data
        })
    }
}
export const sendEmailForResetPassword = (email) => async dispatch => {

    dispatch({
        type: SEND_EMAIL_FOR_RESET_PASSWORD
    })
    try {
        const {data} = await API.sendEmailForResetPassword(email);
        dispatch({
            type: SEND_EMAIL_FOR_RESET_PASSWORD_SUCCESS,
            data
        })

    } catch (error) {
        dispatch({
            type: SEND_EMAIL_FOR_RESET_PASSWORD_ERROR,
            error: error.response?.data
        })
    }
}
export const resetPassword = (id, passwordToken, formData, history) => async dispatch => {
    dispatch({
        type: RESET_PASSWORD
    })
    try {
        const {data} = await API.resetPassword(id, passwordToken, formData);

        dispatch({
            type: RESET_PASSWORD_SUCCESS,
            data
        })

        history.push('/auth');
    } catch (error) {
        dispatch({
            type: RESET_PASSWORD_ERROR,
            error: error.response?.data
        })
    }
}