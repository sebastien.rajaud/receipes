import {SET_FORM_VALUES_FOR_EDIT, RESET_FORM_VALUES} from "./constants";

export const setFormValues = (data) => ({
    type: SET_FORM_VALUES_FOR_EDIT,
    data
})

export const resetFormValues = () => ({
    type: RESET_FORM_VALUES
})