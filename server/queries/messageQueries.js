import MessageModel from "../models/messageModel";

export const findMessagePerRoomId = async (id) => {

    return await MessageModel.find({room: id});
}

export const createMessage = async (message, roomId, user) => {
    return await MessageModel.create({
        authorName: `${user.firstName} ${user.lastName}`,
        author: user,
        data: message,
        room: roomId
    })
}