import UserModel from "../models/userModel";
import { v4 as uuidv4 } from 'uuid';

export const createUser = async (body) => {
    const {email, firstName, lastName, city, address} = body;
    const hashedPassword = await UserModel.hashPassword(body.password);
    try {
        const user = new UserModel({
            firstName,
            lastName,
            address,
            city,
            local: {
                email,
                password: hashedPassword,
                emailToken: uuidv4()
            }
        })
        return await user.save();

    } catch (error) {
        throw(error)
    }
}

export const findUserPerId = (id) => {
    return UserModel.findById(id).exec();
}

export const findUserPerEmail = (email) => {
    return UserModel.findOne({'local.email': email}).exec();
}
export const updateUser = (id, data) => {
    return UserModel.findByIdAndUpdate(id,{...data}, {new: true})
}


