import receipeModel from "../models/receipeModel";
import UserModel from "../models/userModel";
import mongoose from 'mongoose';

export const createReceipe = async (data, file, user) => {
    try {

        const receipe = new receipeModel({
            ...data, picture: file, author: {
                _id: user._id,
                firstName: user.firstName,
                lastName: user.lastName,
                avatarUrl: user?.avatar?.path
            }
        })
        return await receipe.save();
    } catch (error) {
        throw(error)
    }

}

export const updateReceipe = async (data, file, id) => {
    try {
        const match = await receipeModel.findById(id);

        if (match) {
            if (file) {
                return await receipeModel.findByIdAndUpdate(id, {...data, picture: file}, {new: true})
            }
            return await receipeModel.findByIdAndUpdate(id, {...data}, {new: true})
        }
    } catch (error) {
        throw(error)
    }

}

export const deleteReceipe = async (id) => {
    try {
        return receipeModel.findByIdAndDelete(id)

    } catch (error) {
        throw(error)
    }
}


export const getReceipes = async () => {
    try {
        return await receipeModel.find().exec();

    } catch (error) {
        throw(error)
    }
}

export const getSelectedReceipe = async (id) => {

    try {
        return await receipeModel.findById(id);
    } catch (error) {
        throw(error);
    }
}

export const addReceipeToUserFavorites = async (id, userId) => {
    try {
        const user = UserModel.findById(userId);
        if (user) {
            const receipe = await receipeModel.findById(id);

            const exist = receipe.followers.some(el => el.toString() === userId.toString());
            if (!exist) {
                return await receipeModel.findByIdAndUpdate(id, {$push: {"followers": userId}}, {new: true})
            } else {
                throw new Error('Cette recette est déjà ajoutée aux favoris')
            }

        } else {
            throw new Error('Cet utilisateur n\'existe pas')

        }

    } catch (error) {
        throw(error)
    }
}

export const removeReceipeToUserFavorites = async (id, userId) => {
    try {
        const user = UserModel.findById(userId);
        if (user) {
            const receipe = await receipeModel.findById(id);

            const exist = receipe.followers.some(el => el.toString() === userId.toString());
            if (exist) {
                return await receipeModel.findByIdAndUpdate(id, {$pull: {"followers": userId}}, {new: true})
            } else {
                throw new Error('Cette recette n\'est pas dans vos favoris')
            }

        } else {
            throw new Error('Cet utilisateur n\'existe pas')

        }

    } catch (error) {
        throw(error)
    }
}


export const getReceipesByUser = async (id) => {
    return await receipeModel.find({'author._id': id})
}

export const getFavoritesReceipesByUser = async (id) => {
    return await receipeModel.find({followers:  mongoose.Types.ObjectId(id) } )
}