import NamespaceModel from "../models/namespaceModel";

export const getAllNamespaces = async () => {
    return await NamespaceModel.find({}).exec();
}