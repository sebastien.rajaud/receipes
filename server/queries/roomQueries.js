import RoomModel from "../models/roomModel";

export const getAllRoomsByNamespaceID = async (id) => {
    return await RoomModel.find({namespace: id}).sort({index: 1}).exec();
}