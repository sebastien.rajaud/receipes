import passport from 'passport';
import local from 'passport-local';
import {findUserPerEmail, findUserPerId} from "../queries/userQueries";

const LocalStrategy = local.Strategy;


passport.serializeUser((user, done) => {

    done(null, user._id)
})

passport.deserializeUser(async (id, done) => {

    try {
        const user = await (findUserPerId(id));
        if (user) {
            done(null, user);
        }

    } catch (error) {
        done(error)
    }
})

passport.use('local',
    new LocalStrategy({
        usernameField: 'email'
    }, async (email, password, done) => {
        try {
            const user = await findUserPerEmail(email);
            if (user) {
                const match = await user.comparePassword(password)
                if (match) {
                    done(null, user);
                } else {
                    done(null, false, {message: 'Mot de passe erroné.'})
                }
            } else {
                done(null, false, {message: 'Aucun compte n\'existe avec cet email.'})
            }

        } catch (error) {
            done(error)
        }
    })
)
