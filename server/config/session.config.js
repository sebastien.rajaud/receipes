import app from '../index';
import session from 'express-session';
import express from 'express';
import MongoStore from "connect-mongo";
import dotenv from 'dotenv';

dotenv.config();
app.use(session({
    secret: 'the winter comes',
    resave: true,
    saveUninitialized: true,
    cookie: {
        httpOnly: false,
        maxAge: 1000 * 60 * 60 * 24 * 14
    },
    store: MongoStore.create({
        mongoUrl: process.env.CONNECTION_URL,
        ttl: 60 * 60 * 24 * 14
    })
}))

