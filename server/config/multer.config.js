import multer from 'multer';
import path from 'path';

export const uploadAvatar = multer({
    storage: multer.diskStorage({
        destination: (req,file, cb) => {
            cb(null,'public/uploads/avatar')
        },
        filename: (req,file, cb) => {
            const extension = file.mimetype.split('/')[1];
            cb(null, `${file.fieldname}-${Date.now()}.${extension}`)
        }
    })
})


export const uploadReceipes = multer({
    storage: multer.diskStorage({
        destination: (req,file, cb) => {
            cb(null,'public/uploads/receipes')
        },
        filename: (req,file, cb) => {
            const extension = file.mimetype.split('/')[1];
            cb(null, `${file.fieldname}-${Date.now()}.${extension}`)
        }
    })
})