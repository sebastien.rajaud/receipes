import {getAllNamespaces} from "../queries/namespaceQueries";

const server = '../index';
import {Server} from 'socket.io';
import {getAllRoomsByNamespaceID} from "../queries/roomQueries";
import {createMessage, findMessagePerRoomId} from "../queries/messageQueries";

let ios;
let namespaces;

let rooms;
let activeNamespace;
let activeRoom;

import(server).then(s => {
    ios = new Server(s.server, {
        allowRequest: async (request, success) => {
            success(null, true);
        },
        cors: {
            origin: "http://localhost:3000",
            methods: ["GET", "POST", "PUT"],
            credentials: true,
        }
    })
    ios.on('connect', socket => {
        socket.emit('namespaces', namespaces);
    })

    ios.on('close', socket => {
        socket.disconnect(true);
    })

    const initNamespaces = async () => {
        try {
            namespaces = await getAllNamespaces();
            for (let namespace of namespaces) {
                const ns = ios.of(`/${namespace._id}`);
                ns.on('connect', async (nsSocket) => {

                    try {
                        const rooms = await getAllRoomsByNamespaceID(namespace._id);
                        nsSocket.emit('rooms', rooms);
                    } catch (e) {
                        throw e;
                    }

                    nsSocket.on('joinRoom', async (roomId) => {
                        try {
                            nsSocket.join(`/${roomId}`)

                            const messages = await findMessagePerRoomId(roomId);
                            nsSocket.emit('history', messages);
                        } catch (e) {
                            throw (e)
                        }
                    })
                    nsSocket.on("leaveRoom", (roomId) => {
                        nsSocket.leave(`/${roomId}`);
                    });
                    nsSocket.on('message', async (message, roomId, user) => {
                        const newMessage = await createMessage(message, roomId, user);
                        ns.to(`/${roomId}`).emit('message', newMessage)
                    })

                })
            }
        } catch (e) {
            throw e
        }
    }


    initNamespaces();


})

