export const ensureIsAuthenticated = (req, res, next) => {
    if (req.isAuthenticated()) {
        next();
    } else {
        res.status(403).json({error: 'Vous n\'êtes pas autorisé à accéder à cette page.'})
    }
}