import mongoose from 'mongoose';
import NamespaceModel from "../models/namespaceModel.js";
import RoomModel from "../models/roomModel.js";


mongoose.connect('mongodb+srv://receipesdatabase:XZp31e8dA3lg26Zj@cluster0.y3stc.mongodb.net/app?retryWrites=true&w=majority', {useNewUrlParser: true, useUnifiedTopology: true})
    .then(() => {
        console.log('Connexion SEED OK')
        const ns1 = new NamespaceModel({
            title: 'Cuisine'
        })

        const ns2 = new NamespaceModel({
            title: 'Jardinage'
        })

        const ns3 = new NamespaceModel({
            title: 'Series TV'
        })

        ns1
            .save()
            .then( namespace => {
                const room1 = new RoomModel({
                    namespace: namespace._id,
                    index: 0,
                    title: 'Général'
                })
                const room2 = new RoomModel({
                    namespace: namespace._id,
                    index: 1,
                    title: 'Hors sujet'
                })

                Promise.all([room1.save(), room2.save()]).then( () => {
                    console.log('Room du namespace ns1 sauvegardées')
                })
            })

        ns2
            .save()
            .then( namespace => {
                const room1 = new RoomModel({
                    namespace: namespace._id,
                    index: 0,
                    title: 'Général'
                })
                const room2 = new RoomModel({
                    namespace: namespace._id,
                    index: 1,
                    title: 'Hors sujet'
                })

                Promise.all([room1.save(), room2.save()]).then( () => {
                    console.log('Room du namespace ns1 sauvegardées')
                })
            })


        ns3
            .save()
            .then( namespace => {
                const room1 = new RoomModel({
                    namespace: namespace._id,
                    index: 0,
                    title: 'Général'
                })
                const room2 = new RoomModel({
                    namespace: namespace._id,
                    index: 1,
                    title: 'Hors sujet'
                })

                Promise.all([room1.save(), room2.save()]).then( () => {
                    console.log('Room du namespace ns1 sauvegardées')
                })
            })


    })
    .catch(e => console.log(e))
