import express from 'express';
import {getNamespaces} from  '../controllers/tchatController'
import {getRoomsByNamepaceId, sendMessage} from  '../controllers/tchatController'

const router = express.Router();

router.post('/rooms/:roomId/', sendMessage);
router.get('/namespaces/:id/rooms/', getRoomsByNamepaceId);
router.get('/namespaces', getNamespaces);

export default router;