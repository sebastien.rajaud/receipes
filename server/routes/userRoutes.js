import express from 'express';
import {
    signIn,
    signUp,
    logout,
    getUserProfile,
    auth,
    editUserProfile,
    verifyEmail,
    sendEmailForResetPassword,
    resetPasswordForm,
    resetPassword
} from "../controllers/userController";
import {uploadAvatar} from '../config/multer.config';

const router = express.Router();

router.post('/signin', signIn);
router.post('/signup', signUp);
router.post('/logout', logout);
router.get('/auth', auth);
router.post('/auth/forgot-password', sendEmailForResetPassword);
router.get('/auth/reset-password/:userId/:passwordToken', resetPasswordForm);
router.post('/auth/reset-password/:id/:passwordToken', resetPassword);
router.get('/email-verification/:userId/:token', verifyEmail);
router.get('/profile/:id', getUserProfile)

router.put('/profile/:id', uploadAvatar.single('avatar'), editUserProfile)

export default router;