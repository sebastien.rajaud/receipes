import express from 'express';
import userRoutes from './userRoutes';
import receipeRoutes from './receipeRoutes';
import tchatRoutes from './tchatRoutes';
const router = express.Router();

router.use('/user', userRoutes);
router.use('/receipe', receipeRoutes);
router.use('/tchat', tchatRoutes);

export default router;