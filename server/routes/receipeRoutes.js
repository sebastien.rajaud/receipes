import express from 'express';
const router = express.Router();
import {addReceipe, getReceipes, updateReceipe, deleteReceipe, getSelectedReceipe, addReceipeToFavorite, removeReceipeToFavorite, getReceipesPerUser, getFavoritesReceipes} from '../controllers/receipeController';
import {uploadReceipes } from '../config/multer.config';

router.put('/addFavorites',addReceipeToFavorite );
router.put('/removeFavorites',removeReceipeToFavorite );
router.delete('/:id', deleteReceipe);
router.get('/:id', getSelectedReceipe);
router.put('/:id', uploadReceipes.single('picture'), updateReceipe);
router.post('/add', uploadReceipes.single('picture'), addReceipe);
router.get('/user/:id/favorites', getFavoritesReceipes)
router.get('/user/:id', getReceipesPerUser)
router.get('/', getReceipes);

export default router;