import fs from 'fs';
import nodemailer from "nodemailer";
import sparkPostTransport from "nodemailer-sparkpost-transport";
import path from 'path';
import dotenv from 'dotenv';
import pug from 'pug';

dotenv.config();

class Email {

    prodTransporter;
    devTransporter;
    from;
    protocol;

    constructor() {
        this.from = "Receipe App <no-reply@askfordev.fr>";
        this.protocol = process.env.NODE_ENV === 'production' ? 'https' : 'http';
        this.prodTransporter = nodemailer.createTransport(
            sparkPostTransport({
                sparkPostApiKey: process.env.SPARKPOST_API_KEY,
            })
        )
        this.devTransporter = nodemailer.createTransport({
            host: "smtp.mailtrap.io",
            port: 2525,
            auth: {
                user: "91b659a5239f75",
                pass: "c6885144a0e826"
            }
        });

        this.transporter = process.env.NODE_ENV === 'production' ? this.prodTransporter : this.devTransporter;
    }

    async getTemplate(templateName, options) {
        try {

            const template = pug.renderFile(
                path.resolve('email/emails-templates', `${templateName}.pug`),
                options.metadata
            );

            const data = await this.transporter.sendMail({
                from: this.from,
                to: options.to,
                subject: options.subject,
                html: template
            })
        } catch (e) {
            throw new Error(e);
        }

    }

    async sendEmailUpdateProfile(options) {
        try {
            const email = {
                from: this.from,
                to: options.to,
                subject: "Mise à jour de votre profil",
                html: pug.renderFile(
                    path.resolve('email/emails-templates', 'email-edit-profile.pug'), {
                        firstName: options.firstName,
                        lastName: options.lastName,
                    }
                )
            }
            const response = await this.transporter.sendMail(email);
            console.log(response);
        } catch (error) {
            console.log(error)
        }
    }

    async sendEmailWelcome(options) {
        try {
            const email = {
                from: this.from,
                to: options.to,
                subject: "Bienvenue sur Receipes App",
                html: pug.renderFile(
                    path.resolve('email/emails-templates', 'email-welcome.pug'), {
                        firstName: options.firstName,
                        lastName: options.lastName,
                    }
                )
            }
            const response = await this.transporter.sendMail(email);
            console.log(response);
        } catch (error) {
            console.log(error)
        }
    }

    async sendEmailVerification(options) {
        try {
            const email = {
                from: this.from,
                to: options.to,
                subject: "Verification de votre adresse mail",
                html: pug.renderFile(
                    path.resolve('email/emails-templates', 'email-verification.pug'), {
                        firstName: options.firstName,
                        lastName: options.lastName,
                        url: `${this.protocol}://${options.host}/user/email-verification/${options.userId}/${options.token}`,
                    }
                )
            }

            const response = await this.transporter.sendMail(email);

        } catch (error) {
            console.log(error)
        }
    }

    async sendEmailForResetPassword(options) {
        const email = {
            from: this.from,
            to: options.to,
            subject: "Réinitialisation de votre mot de passe",
            html: pug.renderFile(
                path.resolve('email/emails-templates', 'email-reset-password.pug'), {
                    url: `${this.protocol}://${options.host}/user/auth/reset-password/${options.userId}/${options.passwordToken}`
                })
        }
        await this.transporter.sendMail(email);
    }
}

export default new Email();