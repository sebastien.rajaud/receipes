import express from 'express';
import bodyParser from 'body-parser';
import cors from 'cors';
import indexRoutes from './routes/index';
import dotenv from 'dotenv';
import morgan from 'morgan';
import passport from 'passport';
import session from "express-session";
import MongoStore from "connect-mongo";
import './database/dbConnection';
import cookieParser from 'cookie-parser';

dotenv.config();
import './config/passport.config';

import path from 'path';
import {fileURLToPath} from 'url';

const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);


export const app = express();


app.use(cors({credentials: true, origin: ['http://localhost:3000', 'http://localhost:5000']}))

app.use(bodyParser.json({limit: '30mb', extended: true}))
app.use(bodyParser.urlencoded({limit: '30mb', extended: true}))
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use(session({
    path: '/',
    secret: 'the winter comes',
    resave: false,
    saveUninitialized: false,
    cookie: {
        httpOnly: false,
        secure: false,
        maxAge: 4 * 60 * 60 * 1000
    },
    store: MongoStore.create({
        mongoUrl: process.env.CONNECTION_URL,
        ttl: 60 * 60 * 24 * 14
    })
}));

app.use(morgan('dev'));

// Passport
app.use(passport.initialize());
app.use(passport.session());

// Routes
app.use(indexRoutes);

export const server = app.listen(process.env.PORT, () => {
    console.log(`Server running on port : ${process.env.PORT}`)
})

import('./config/socket.config');

