import {
    createReceipe, getReceipes as getAllReceipes,
    updateReceipe as editReceipe, deleteReceipe as deleteReceipeById,
    getSelectedReceipe as getReceipeById, addReceipeToUserFavorites,
    removeReceipeToUserFavorites,
    getReceipesByUser,
    getFavoritesReceipesByUser

} from '../queries/receipeQueries';
import {ensureUserIsAuthorized} from './userController';


export const addReceipe = async (req, res, next) => {
    const {body} = req;
    const {file} = req;

    try {


        if (!body.time || isNaN(body.time)) {
            return res.status(401).send('Vous devez renseigner une durée en minutes au format numérique.');
        }

        if (!body.title || !body.title.length) {
            return res.status(401).send('Vous devez renseigner un titre.');
        }

        if (!file) {
            return res.status(401).send('Vous devez ajouter une image.');
        }

        const receipe = await createReceipe(body, file, req.user)

        return res.status(200).json({receipe});
    } catch (error) {
        console.log(error)
        return res.status(500).json({error: error.message})
    }
}

export const updateReceipe = async (req, res, next) => {
    const {body} = req;
    const {file} = req;
    const {id} = req.params;
    const author = JSON.parse(body.author)

    try {
        if (!ensureUserIsAuthorized(author, req)) {
            return res.status(403).send('Vous n\'avez pas l\'autorisation de modifer ce contenu');
        }

        if (!body.time || isNaN(body.time)) {
            return res.status(401).send('Vous devez renseigner une durée en minutes au format numérique.');
        }

        if (!body.title || !body.title.length) {
            return res.status(401).send('Vous devez renseigner un titre.');
        }


        let data = null;
        if (!file) {
            data = {...body, picture: JSON.parse(req.body.picture), author}
        } else {
            data = {...body, author}
        }
        const receipe = await editReceipe(data, file, id);

        return res.status(200).json({receipe});
    } catch (error) {
        return res.status(403).send(error.message)
    }

}
export const deleteReceipe = async (req, res, next) => {
    const {id} = req.params;

    try {
        const deletedReceipe = await deleteReceipeById(id);
        res.status(200).json({receipe: deletedReceipe});
    } catch (error) {
        res.status(401).json({error: error.message})
    }
}
export const getReceipes = async (req, res, next) => {
    try {
        const receipes = await getAllReceipes();

        res.status(200).json({receipes: receipes})

    } catch (error) {
        res.status(error.statusCode).json({error: error.message})
    }
}

export const getSelectedReceipe = async (req, res, next) => {
    const {id} = req.params;

    try {
        const selectedReceipe = await getReceipeById(id)
        res.status(200).json({selectedReceipe})
    } catch (error) {
        res.status(401).json({error: error.message})
    }

}


export const addReceipeToFavorite = async (req, res, next) => {
    const {receipeId} = req.body;


    try {
        if (!req.user) {
            res.status(403).send('Vous n etes pas authentifié');
        }
        const likedReceipe = await addReceipeToUserFavorites(receipeId, req.user._id);
        return res.status(200).json({likedReceipe})
    } catch (error) {
        res.status(401).json({error: error.message})
    }
}

export const removeReceipeToFavorite = async (req, res, next) => {
    const {receipeId} = req.body;
    try {
        if (!req.user) {
            res.status(403).send('Vous n etes pas authentifié');
        }
        const disLikedReceipe = await removeReceipeToUserFavorites(receipeId, req.user._id);
        return res.status(200).json({disLikedReceipe})
    } catch (error) {
        res.status(401).json({error: error.message})
    }
}

export const getReceipesPerUser = async (req,res) => {
    try {
        const {id} = req.params;
        const userReceipes = await getReceipesByUser(id)
        res.status(200).json({userReceipes})
    } catch (error) {
        res.status(400).send(error.message)
    }
}

export const getFavoritesReceipes = async (req,res) => {
    try {
        const {id} = req.params;
        const favoritesReceipes = await getFavoritesReceipesByUser(id)
        res.status(200).json({favoritesReceipes})
    } catch (error) {
        res.status(400).send(error.message)
    }
}