import {getAllNamespaces} from "../queries/namespaceQueries";
import {getAllRoomsByNamespaceID} from "../queries/roomQueries";
import MessageModel from "../models/messageModel";
import {createMessage} from "../queries/messageQueries";

export const getNamespaces = async (req, res, next) => {

    try {
        const namespaces = await getAllNamespaces();
        return res.status(200).json({namespaces})
    } catch (e) {
        console.log(e)
    }
}

export const getRoomsByNamepaceId = async (req, res, next) => {
    try {

        const {id} = req.params;

        const rooms = await getAllRoomsByNamespaceID(id)
        return res.status(200).json({rooms})
    } catch (e) {
        console.log(e)
    }
}

export const sendMessage = async (req, res) => {
    try {
        const {message} = req.body;
        const {roomId} = req.params;
        const newMessage = await createMessage(message, roomId, req.user);

        return res.status(200).json({message: newMessage})

    } catch (e) {
        console.log(e)
    }
}