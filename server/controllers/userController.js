import {createUser, findUserPerEmail, findUserPerId, updateUser } from '../queries/userQueries';

import passport from 'passport';
import UserModel from "../models/userModel";
import email from "../email/email";
import {v4 as uuidv4} from 'uuid';
import moment from 'moment-timezone';



export const signIn = (req, res, next) => {
    passport.authenticate('local', async (err, user, info) => {
        const {email, password} = req.body;
        if (err) {
            next(err)
        }

        if (!email || !password) {
            return res.status(401).send('Il manque des identifiants');
        }
        if (!user) {
            return res.status(401).send(info?.message);
        } else {
            req.logIn(user, err => {
                if (err) {
                    next(err)
                }
                return res.json({user, isAuthenticated: req.isAuthenticated()})
            })

        }
    })(req, res, next)
}

export const signUp = async (req, res, next) => {

    const {firstName, password, confirmPassword} = req.body;


    try {
        if (password !== confirmPassword) {
            throw new Error('Les mots de passes ne sont pas identiques')
        }
        if (!firstName || firstName === '') {
            throw new Error('Veuillez renseigner votre prénom')
        }
        const newUser = await createUser(req.body);
        if (newUser) {
            email.sendEmailVerification({
                to: newUser.local.email,
                firstName: newUser.firstName,
                lastName: newUser.lastName,
                userId: newUser._id,
                token: newUser.local.emailToken,
                host: req.headers.host
            })
        }
        req.login(newUser, function (err) {
            if (err) {
                res.status(500).json({message: 'Une erreur est survenue.'});
                return;
            }
            return res.status(200).json({user: newUser, isAuthenticated: req.isAuthenticated()})
        });

    } catch (e) {
        if (e.code === 11000) {
            return res.status(401).send('Cette adresse email est déjà utilisée par un autre compte')
        } else {
            res.status(401).send(e.message)
        }
    }
}

export const logout = (req, res) => {
    req.logOut();
    res.json({user: null, isAuthenticated: req.isAuthenticated()})
}

export const auth = (req, res) => {
    if (req.user && req.isAuthenticated()) {
        return res.json({user: req.user, isAuthenticated: req.isAuthenticated()})
    }
    return res.json({user: null, isAuthenticated: req.isAuthenticated()})
}

export const getUserProfile = async (req, res) => {
    const {id} = req.params;
    try {
        const user = await findUserPerId(id);
        if (user) {
            res.json({user: user});
            return;
        }
    } catch (e) {
        res.status(e.statusCode).send(e.message)
    }
}

export const editUserProfile = async (req, res) => {
    const {id} = req.params;
    const data = req.body;
    try {
        const user = await findUserPerId(id);

        if (user) {
            const local = JSON.parse(data.local)
            const userData = {...data, local}

            const user = await updateUser(id, {...userData, avatar: req.file}, {new: true})
            if (user) {
                email.sendEmailUpdateProfile({
                    to: user.local.email,
                    firstName: user.firstName,
                    lastName: user.lastName,
                })
            }

            res.status(200).json({user: user, isAuthenticated: req.isAuthenticated()})
        }
    } catch (e) {
        res.status(e.statusCode).send(e.message)
    }
}

export const ensureUserIsAuthorized = (author, req) => {
    console.log(author._id)
    if (author._id.toString() === req.user._id.toString()) {
        return true;
    }
    return false;
}

export const verifyEmail = async (req, res, next) => {
    try {
        const {userId, token} = req.params;
        const user = await findUserPerId(userId)
        if (user && user.local.emailToken === token) {
            user.local.emailVerified = true;
            await user.save()
            res.redirect(`${process.env.PROTOCOL}://${process.env.HOSTNAME}/user/profile/${user._id}`);
        } else {
            return res.status(400).send('Probleme durant la vérification de l\'email');
        }
    } catch (error) {
        res.status(403).send(error.message)
    }
}

export const sendEmailForResetPassword = async (req, res, next) => {
    try {
        const userEmail = req.body.email;
        const user = await findUserPerEmail(userEmail);
        if (!user || !userEmail) {
            return res.status(401).send('Il n\'y a aucun compte avec cette adresse email')
        }
        const passwordToken = uuidv4();
        user.local.passwordToken = passwordToken;
        user.local.passwordTokenExpiration = moment().add(2, 'hours').format();

        await user.save();

        await email.sendEmailForResetPassword({
            to: user.local.email,
            userId: user._id,
            passwordToken: passwordToken,
            host: req.headers.host
        })


        return res.status(200).json({user})
    } catch (error) {
        res.status(400).send(error.message)
    }
}

export const resetPasswordForm = async (req, res, next) => {

    try {
        const {userId, passwordToken} = req.params;

        const user = await findUserPerId(userId);

        if (!user) {
            return res.status(401).send('Il n\'y a aucun compte avec cet identifiant')
        }

        if (user.local.passwordToken !== passwordToken) {
            return res.status(401).send('Le token n\'est pas valide')
        }

        res.redirect(`${process.env.PROTOCOL}://${process.env.HOSTNAME}/auth/reset-password/${userId}/${passwordToken}`);

    } catch (error) {
        res.status(400).send(error.message)
    }
}

export const resetPassword = async (req, res, next) => {
    try {
        const {password, confirmPassword} = req.body;
        const {id, passwordToken} = req.params;

        if (password && password !== confirmPassword) {
            res.status(400).send('Les mots de passe ne sont pas identiques ou non remplis')
        }

        const user = await findUserPerId(id);

        if (!user) {
            return res.status(400).send('Il n\'y pas de compte avec cet identifiant');
        }

        if (user.local.passwordToken !== passwordToken) {
            return res.status(401).send('Le token n\'est pas valide');
        }

        if (moment().format() > moment(user.local.passwordTokenExpiration).format()) {
            return res.status(401).send('Le token n\'est PLUS valide');
        }

        user.local.password = await UserModel.hashPassword(password);
        user.local.passwordToken = null;
        user.local.passwordTokenExpiration = null;

        await user.save();

        return res.status(200).json({user})

    } catch (error) {
        res.status(400).send(error.message)
    }
}


