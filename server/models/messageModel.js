import mongoose from 'mongoose';

const messageSchema = mongoose.Schema({
    authorName: String,
    author: {type: mongoose.Schema.Types.ObjectId, ref: 'user'},
    data: {type: String, required: true},
    room: {type: mongoose.Schema.Types.ObjectId, ref: 'room'}
}, {timestamp: true});

const MessageModel = mongoose.model('message', messageSchema);

export default MessageModel;