import mongoose from 'mongoose';

const roomSchema = mongoose.Schema({
    title: String,
    index: {type: Number},
    namespace: {type: mongoose.Schema.Types.ObjectId, ref: 'namespace'}
});

const RoomModel = mongoose.model('room', roomSchema);

export default RoomModel;