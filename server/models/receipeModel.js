import mongoose from 'mongoose';
import UserModel, {userSchema} from "./userModel";

const receipeSchema = mongoose.Schema({
    title: {type: String, required: true},
    time: {type: Number},
    ingredients: {type: String},
    instructions: {type: String},
    picture: {type: Object},
    author: {
        _id: {
            type: mongoose.Schema.Types.ObjectId,
            ref: UserModel,
            required: true
        },
        firstName: {type: String},
        lastName: {type: String},
        avatarUrl: {type:String}
    },
    followers : [{type: mongoose.Schema.Types.ObjectId, ref:'User'}]
}, { timestamps: true })

const receipeModel= mongoose.model('receipe', receipeSchema);

export default receipeModel;