import mongoose from 'mongoose';
import bcrypt from 'bcrypt';

export const userSchema = mongoose.Schema({
    firstName: {type: String, required: true},
    lastName: {type: String, required: true},
    address: {type: String},
    city: String,
    local: {
        email: {type: String, required: true, unique: true},
        emailVerified: {type: Boolean, default: false},
        password: {type: String, required: true},
        emailToken: {type: String},
        passwordToken: {type: String},
        passwordTokenExpiration: {type: String}
    },
    avatar: {type: Object}
});

userSchema.statics.hashPassword = (password) => {
    return bcrypt.hash(password, 10)
}

userSchema.methods.comparePassword = function (password) {
    return bcrypt.compare(password, this.local.password)
}

const UserModel = mongoose.model('user', userSchema);

export default UserModel;