import mongoose from 'mongoose';

const namespaceSchema = mongoose.Schema({
    title: String,
});

const NamespaceModel = mongoose.model('namespace', namespaceSchema);

export default NamespaceModel;