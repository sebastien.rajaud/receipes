import mongoose from 'mongoose';
import dotenv from 'dotenv';

dotenv.config();


mongoose.connect(process.env.CONNECTION_URL, {useNewUrlParser: true, useUnifiedTopology: true})
    .then(() => console.log('Connexion DB OK'))
    .catch(e => console.log(e.message))

mongoose.set('useFindAndModify', false);

